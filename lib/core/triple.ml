(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Subject = struct
  type internal
  type t = internal Term.internal

  let of_iri iri = Term.Iri iri
  let of_blank_node bnode = Term.Blank_node bnode

  let map f_iri f_bnode =
    Term.map f_iri f_bnode
      (* Note that it is "safe" to raise an exception because we guarantee that
         this case never occurs when using the library. *)
      (fun _ -> raise @@ Invalid_argument "subject can not be a literal")

  let to_term s = map Term.of_iri Term.of_blank_node s
  let to_iri s = map Option.some (fun _ -> None) s
  let to_blank_node s = map (fun _ -> None) Option.some s
  let equal s1 s2 = Term.equal (to_term s1) (to_term s2)
  let compare s1 s2 = Term.compare (to_term s1) (to_term s2)
  let pp ppf s = Fmt.pf ppf "@[<4><subject@ %a>@]" Term.pp s
end

module Predicate = struct
  type internal
  type t = internal Term.internal

  let of_iri iri = Term.Iri iri

  let map f =
    Term.map f
      (fun _ -> raise @@ Invalid_argument "predicate can not be a blank node")
      (fun _ -> raise @@ Invalid_argument "predicate can not be a literal")

  let to_term p = map Term.of_iri p
  let to_iri p = map (fun x -> x) p
  let equal p1 p2 = Term.equal (to_term p1) (to_term p2)
  let compare p1 p2 = Term.compare (to_term p1) (to_term p2)
  let pp ppf p = Fmt.pf ppf "@[<4><predicate@ %a>@]" Term.pp p
end

module Object = struct
  type internal
  type t = internal Term.internal

  let of_iri iri = Term.Iri iri
  let of_blank_node bnode = Term.Blank_node bnode
  let of_literal literal = Term.Literal literal

  let of_term = function
    (* to trick the type system we need to unpack and repack. *)
    | Term.Iri iri -> Term.Iri iri
    | Term.Blank_node bnode -> Term.Blank_node bnode
    | Term.Literal literal -> Term.Literal literal

  let map = Term.map
  let to_term o = map Term.of_iri Term.of_blank_node Term.of_literal o
  let to_iri o = map Option.some (fun _ -> None) (fun _ -> None) o
  let to_blank_node o = map (fun _ -> None) Option.some (fun _ -> None) o
  let to_literal o = map (fun _ -> None) (fun _ -> None) Option.some o
  let equal o1 o2 = Term.equal (to_term o1) (to_term o2)
  let compare o1 o2 = Term.compare (to_term o1) (to_term o2)
  let pp ppf o = Fmt.pf ppf "@[<4><object@ %a>@]" Term.pp o
end

type t = { subject : Subject.t; predicate : Predicate.t; object' : Object.t }

let make subject predicate object' = { subject; predicate; object' }

let equal { subject = s1; predicate = p1; object' = o1 }
    { subject = s2; predicate = p2; object' = o2 } =
  Subject.equal s1 s2 && Predicate.equal p1 p2 && Object.equal o1 o2

let pp ppf triple =
  Fmt.pf ppf "@[<8><triple@ %a@ %a@ %a>@]@." Term.pp triple.subject Term.pp
    triple.predicate Term.pp triple.object'
