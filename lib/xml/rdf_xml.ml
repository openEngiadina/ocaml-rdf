(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(* TODO: this could use some cleanup and performance improvement... *)

let rdf local = ("http://www.w3.org/1999/02/22-rdf-syntax-ns#", local)

type ctx = {
  base_iri : Rdf.Iri.t option;
  lang : string option;
  (* see 7.4 List Expansion Rules *)
  list_expansion_counter : int;
  (* random seed for blank node generation *)
  seed : Random.State.t;
  (* list of rdf:ID that have already been used. RDF/XML does not
     allow an id to be used multiple times. *)
  used_ids : Rdf.Iri.t list;
}

module OrderedName = struct
  type t = Xmlm.name

  let compare a b =
    (* TODO figure out why first version doesn't work *)
    (* if compare (fst a) (fst b) == 0 then compare (snd a) (snd b) else 0 *)
    compare (fst a ^ snd a) (fst b ^ snd b)
end

module NameSet = Set.Make (OrderedName)
module NameMap = Map.Make (OrderedName)

(* https://www.w3.org/TR/rdf-syntax-grammar/#coreSyntaxTerms *)
let core_syntax_terms =
  NameSet.of_list
    [
      rdf "RDF";
      rdf "ID";
      rdf "about";
      rdf "parseType";
      rdf "resource";
      rdf "nodeID";
      rdf "datatype";
    ]

let old_terms =
  NameSet.of_list [ rdf "aboutEach"; rdf "aboutEachPrefix"; rdf "bagID" ]

let is_valid_node_element_name name =
  not
    (NameSet.mem name core_syntax_terms
    || name = rdf "li"
    || NameSet.mem name old_terms)

let resolve ctx rel =
  match ctx.base_iri with
  | Some base_iri -> Rdf.Iri.resolve "http" base_iri (Rdf.Iri.of_string rel)
  | None -> Rdf.Iri.of_string rel

let bind_none f opt = match opt with Some v -> Some v | None -> f ()
let get_attribute attributes name = List.assoc_opt name attributes
let get_rdf_parse_type attributes = List.assoc_opt (rdf "parseType") attributes

(* An rdf:ID value must be a valid XML NCName production
   (https://www.w3.org/TR/rdf-syntax-grammar/#rdf-id) *)
let is_valid_ncname s =
  let decode s =
    Uutf.String.fold_utf_8
      (fun res _pos v ->
        match v with
        | `Malformed s ->
            raise
            @@ Invalid_argument
                 (Format.sprintf
                    "Malformed UTF-8 while checking rdf:ID value (%s)" s)
        | `Uchar uchar -> Seq.append res (Seq.return @@ Uchar.to_int uchar))
      Seq.empty s
  in
  (* Following functions are taken from dbuenzli's Xmlm which is distributed under the ISC license. *)
  let r : int -> int -> int -> bool = fun u a b -> a <= u && u <= b in
  let is_white = function
    | 0x0020 | 0x0009 | 0x000D | 0x000A -> true
    | _ -> false
  in
  let comm_range u =
    (* common to functions below *)
    r u 0x00C0 0x00D6 || r u 0x00D8 0x00F6 || r u 0x00F8 0x02FF
    || r u 0x0370 0x037D || r u 0x037F 0x1FFF || r u 0x200C 0x200D
    || r u 0x2070 0x218F || r u 0x2C00 0x2FEF || r u 0x3001 0xD7FF
    || r u 0xF900 0xFDCF || r u 0xFDF0 0xFFFD || r u 0x10000 0xEFFFF
  in
  let is_name_start_char = function
    (* {NameStartChar} - ':' (XML 1.1) *)
    | u when r u 0x0061 0x007A || r u 0x0041 0x005A -> true (* [a-z] | [A-Z] *)
    | u when is_white u -> false
    | 0x005F -> true (* '_' *)
    | u when comm_range u -> true
    | _ -> false
  in
  let is_name_char = function
    (* {NameChar} - ':' (XML 1.1) *)
    | u when r u 0x0061 0x007A || r u 0x0041 0x005A -> true (* [a-z] | [A-Z] *)
    | u when is_white u -> false
    | u when r u 0x0030 0x0039 -> true (* [0-9] *)
    | 0x005F | 0x002D | 0x002E | 0x00B7 -> true (* '_' '-' '.' *)
    | u when comm_range u || r u 0x0300 0x036F || r u 0x203F 0x2040 -> true
    | _ -> false
  in
  decode s |> List.of_seq
  |> (function
       | head :: rest when is_name_start_char head -> rest
       | _ :: rest ->
           (* 0x002A ('*') is not a valid character and will cause next
              check to fail. *)
           0x002A :: rest
       | l -> l)
  |> List.for_all is_name_char

let node_element_subject ctx (_name, attributes) =
  None
  |> bind_none (fun () ->
         match get_attribute attributes (rdf "ID") with
         | Some id ->
             if not @@ is_valid_ncname id then
               raise
               @@ Invalid_argument
                    (Format.sprintf
                       "rdf:ID value is not a valid XML Name production (%s)" id)
             else
               let resolved = resolve ctx ("#" ^ id) in
               if List.mem resolved ctx.used_ids then
                 raise
                 @@ Invalid_argument
                      (Format.sprintf "Same rdf:ID used twice (%s)" id)
               else
                 ( { ctx with used_ids = resolved :: ctx.used_ids },
                   resolved |> Rdf.Triple.Subject.of_iri )
                 |> Option.some
         | None -> None)
  |> bind_none (fun () ->
         match get_attribute attributes (rdf "nodeID") with
         | Some id ->
             if not @@ is_valid_ncname id then
               raise
               @@ Invalid_argument
                    (Format.sprintf
                       "rdf:nodeID value is not a valid XML Name production \
                        (%s)"
                       id)
             else
               ( ctx,
                 Rdf.Blank_node.of_string id |> Rdf.Triple.Subject.of_blank_node
               )
               |> Option.some
         | None -> None)
  |> bind_none (fun () ->
         match get_attribute attributes (rdf "about") with
         | Some iri ->
             (ctx, resolve ctx iri |> Rdf.Triple.Subject.of_iri) |> Option.some
         | None -> None)
  |> Option.value
       ~default:
         ( ctx,
           Rdf.Blank_node.generate ~seed:ctx.seed ()
           |> Rdf.Triple.Subject.of_blank_node )

let empty_property_element_subject ctx attributes =
  (* gets subject for empty property element (see 7.2.21) *)
  (* Returns both as subject and object *)
  None
  |> bind_none (fun () ->
         match get_attribute attributes (rdf "resource") with
         | Some iri ->
             if Option.is_none @@ get_attribute attributes (rdf "nodeID") then
               Rdf.Triple.Subject.of_iri @@ resolve ctx iri |> Option.some
             else
               raise
               @@ Invalid_argument
                    "encountered rdf:nodeID and rdf:resource in property \
                     element"
         | None -> None)
  |> bind_none (fun () ->
         match get_attribute attributes (rdf "nodeID") with
         | Some id ->
             if is_valid_ncname id then
               Rdf.Triple.Subject.of_blank_node @@ Rdf.Blank_node.of_string id
               |> Option.some
             else
               raise
               @@ Invalid_argument
                    (Format.sprintf
                       "rdf:nodeID value is not a valid XML Name production \
                        (%s)"
                       id)
         | None -> None)
  |> Option.value
       ~default:
         (Rdf.Blank_node.generate ~seed:ctx.seed ()
         |> Rdf.Triple.Subject.of_blank_node)

let is_xmlns (uri, _local) = String.equal uri Xmlm.ns_xmlns

let is_property_attribute_uri name =
  not
    (NameSet.mem name core_syntax_terms
    || name = rdf "Description"
    || name = rdf "li"
    || NameSet.mem name old_terms || is_xmlns name)

let iri_of_name name = Rdf.Iri.of_string (fst name ^ snd name)

let string_literal ctx value =
  (* normalize to normal form C *)
  let value = Uunf_string.normalize_utf_8 `NFC value in
  match ctx.lang with
  | Some lang -> Rdf.Literal.make_string ~language:lang value
  | None -> Rdf.Literal.make_string value

let set_language ctx attributes =
  match
    get_attribute attributes ("http://www.w3.org/XML/1998/namespace", "lang")
  with
  | Some lang -> { ctx with lang = Some lang }
  | None -> ctx

let set_base ctx attributes =
  match
    get_attribute attributes ("http://www.w3.org/XML/1998/namespace", "base")
  with
  | Some base -> { ctx with base_iri = Some (Rdf.Iri.of_string base) }
  | None -> ctx

let is_property_element_uri name =
  not
    (NameSet.mem name core_syntax_terms
    || name = rdf "Description"
    || NameSet.mem name old_terms)

let valid_node_element_attributes attributes =
  let both_node_id_and_about_present =
    (Option.is_some @@ get_attribute attributes (rdf "nodeID"))
    && (Option.is_some @@ get_attribute attributes (rdf "about"))
  in
  let both_node_id_and_id_present =
    (Option.is_some @@ get_attribute attributes (rdf "nodeID"))
    && (Option.is_some @@ get_attribute attributes (rdf "ID"))
  in
  List.for_all
    (fun (name, _) ->
      name = rdf "ID"
      || name = rdf "nodeID"
      || name = rdf "about"
      || is_property_attribute_uri name
      || is_xmlns name)
    attributes
  && (not both_node_id_and_about_present)
  && not both_node_id_and_id_present

(* if parseType is specified then the only other permitted attribute is rdf:ID *)
let valid_parse_type_property_element_attributes attributes =
  List.for_all
    (fun (name, _) -> name = rdf "ID" || name = rdf "parseType")
    attributes

(* reify statement if rdf:ID attribute is present *)
let reify ctx emit_triple attributes triple =
  (* emit reified triples (see 7.3 Reification Rules) *)
  let emit_reification_triples emit_triple subject triple =
    emit_triple
      Rdf.Triple.(
        make subject
          (Predicate.of_iri @@ Rdf.Namespace.rdf "type")
          (Object.of_iri @@ Rdf.Namespace.rdf "Statement"));
    emit_triple
      Rdf.Triple.(
        make subject
          (Predicate.of_iri @@ Rdf.Namespace.rdf "subject")
          (triple.subject |> Subject.to_term |> Object.of_term));
    emit_triple
      Rdf.Triple.(
        make subject
          (Predicate.of_iri @@ Rdf.Namespace.rdf "predicate")
          (triple.predicate |> Predicate.to_term |> Object.of_term));
    emit_triple
      Rdf.Triple.(
        make subject
          (Predicate.of_iri @@ Rdf.Namespace.rdf "object")
          triple.object')
  in
  match List.assoc_opt (rdf "ID") attributes with
  | Some id ->
      if is_valid_ncname id then
        let reification_subject =
          Rdf.Triple.Subject.of_iri @@ resolve ctx ("#" ^ id)
        in
        emit_reification_triples emit_triple reification_subject triple
      else
        raise
        @@ Invalid_argument
             (Format.sprintf
                "rdf:ID value is not a valid XML Name production (%s)" id)
  | None -> ()

let rec node_handle_property_attributes ctx emit_triple subject = function
  | [] -> ()
  | (name, value) :: rest ->
      if name = rdf "type" then
        let object_iri = resolve ctx value in
        emit_triple
          Rdf.Triple.(
            make subject
              (Predicate.of_iri @@ Rdf.Namespace.rdf "type")
              (Object.of_iri object_iri))
      else if is_property_attribute_uri name then
        emit_triple
          Rdf.Triple.(
            make subject
              (Predicate.of_iri @@ iri_of_name name)
              (Object.of_literal @@ string_literal ctx value))
      else if NameSet.mem name old_terms then
        raise
        @@ Invalid_argument
             (Format.asprintf
                "encountered old term in attributes of a property element (%a)"
                Xmlm.pp_name name);

      node_handle_property_attributes ctx emit_triple subject rest

let read_end_element signals =
  match signals () with
  | Seq.Nil ->
      raise @@ Invalid_argument "signals ended while expecting end element"
  | Seq.Cons (`El_end, signals) -> signals
  | _ -> raise @@ Invalid_argument "expecting end element"

let read_literal signals =
  let rec read_children output signals =
    match signals () with
    | Seq.Nil ->
        raise @@ Invalid_argument "signals ended while expecting end element"
    | Seq.Cons (`El_end, signals) ->
        (* return to higher level *)
        signals
    | Seq.Cons (`El_start tag, signals) ->
        (* output the start element *)
        Xmlm.output output (`El_start tag);
        (* recurse into the element *)
        let signals = read_children output signals in
        (* emit the end element (as read_children does not emit end elements itself. *)
        Xmlm.output output `El_end;
        (* continue reading the next element *)
        read_children output signals
    | Seq.Cons (s, signals) ->
        Xmlm.output output s;
        (* continue reading the next element *)
        read_children output signals
  in
  (* We may encounter just a single data element. This is not a valid XML
   * document and must be handled specially. *)
  match signals () with
  | Seq.Cons (`Data d, signals) ->
      (* read the end element *)
      let signals = read_end_element signals in
      (* return the literal value as is *)
      (d, signals)
  | _ ->
      (* create an Xmln output *)
      let buffer = Buffer.create 10 in
      let output = Xmlm.make_output ~decl:false ~nl:false (`Buffer buffer) in

      Xmlm.output output (`Dtd None);

      (* read the child elements *)
      let signals = read_children output signals in
      (* return literal *)
      (Bytes.to_string @@ Buffer.to_bytes buffer, signals)

let lookahead_is_valid_node_element_name signals =
  match signals () with
  | Seq.Cons (`El_start (name, _), _) when is_valid_node_element_name name ->
      true
  | _ -> false

let lookahead_is_data signals =
  match signals () with Seq.Cons (`Data _, _) -> true | _ -> false

let lookahead_is_end signals =
  match signals () with Seq.Cons (`El_end, _) -> true | _ -> false

let rec parse_property_element ctx emit_triple subject signals =
  (* Parses a signle property element and returns a pair with the
     signals and a bool indicating if a property was parsed. This bool
     can be used to decide if there are more property elements to be
     parsed. *)
  match signals () with
  | Seq.Nil ->
      raise @@ Invalid_argument "signals ended while expecting property element"
  | Seq.Cons (`Dtd _, _) -> raise @@ Invalid_argument "unexpected DTD"
  (* 7.2.15 Production resourcePropertyElt *)
  | Seq.Cons (`El_start (name, attributes), signals)
    when is_property_element_uri name
         && (Option.is_none @@ get_rdf_parse_type attributes)
         && lookahead_is_valid_node_element_name signals -> (
      match parse_node_element ctx emit_triple signals with
      | Some object', ctx, signals ->
          let predicate, increment_list_expansion_counter =
            if name = rdf "li" then
              ( Rdf.Namespace.rdf
                  ("_" ^ Int.to_string ctx.list_expansion_counter)
                |> Rdf.Triple.Predicate.of_iri,
                true )
            else (iri_of_name name |> Rdf.Triple.Predicate.of_iri, false)
          in

          (* add triple to connect nested subject *)
          let triple =
            Rdf.Triple.(
              make subject predicate (Object.of_term @@ Subject.to_term object'))
          in
          emit_triple triple;

          (* handle reification *)
          reify ctx emit_triple attributes triple;

          (* read end element (as there can only be one node element) and return *)
          (true, increment_list_expansion_counter, read_end_element signals)
      | None, _ctx, _signals ->
          (* this should not happen as we looked and made sure that it
             is something that is parseable *)
          raise
          @@ Failure
               "value returned by parse_node_element does not match what \
                expected by looking ahead in parse_property_element. Maybe a \
                transient Seq?")
  (* 7.2.16 Production literalPropertyElt *)
  | Seq.Cons (`El_start (name, attributes), signals)
    when lookahead_is_data signals
         && (Option.is_none @@ get_rdf_parse_type attributes) -> (
      match signals () with
      | Seq.Cons (`Data value, signals) ->
          let predicate, increment_list_expansion_counter =
            if name = rdf "li" then
              ( Rdf.Namespace.rdf
                  ("_" ^ Int.to_string ctx.list_expansion_counter)
                |> Rdf.Triple.Predicate.of_iri,
                true )
            else (iri_of_name name |> Rdf.Triple.Predicate.of_iri, false)
          in

          (* set language *)
          let ctx = set_language ctx attributes in

          let object' =
            match get_attribute attributes (rdf "datatype") with
            | Some datatype_string ->
                Rdf.Literal.make value (Rdf.Iri.of_string datatype_string)
                |> Rdf.Triple.Object.of_literal
            | None -> string_literal ctx value |> Rdf.Triple.Object.of_literal
          in

          (* emit the triple *)
          let triple = Rdf.Triple.(make subject predicate object') in
          emit_triple triple;

          (* handle reification *)
          reify ctx emit_triple attributes triple;

          (* read end element (only a single data element is allowed) *)
          (true, increment_list_expansion_counter, read_end_element signals)
      | _ ->
          raise
          @@ Failure
               "When looking ahead we saw a Data element, now we get something \
                else. What is this? A transient Seq?")
  (* 7.2.17 Production parseTypeLiteralPropertyElt *)
  | Seq.Cons (`El_start (name, attributes), signals)
    when get_rdf_parse_type attributes = Some "Literal"
         && valid_parse_type_property_element_attributes attributes ->
      let predicate, increment_list_expansion_counter =
        if name = rdf "li" then
          ( Rdf.Namespace.rdf ("_" ^ Int.to_string ctx.list_expansion_counter)
            |> Rdf.Triple.Predicate.of_iri,
            true )
        else (iri_of_name name |> Rdf.Triple.Predicate.of_iri, false)
      in

      (* read the literal value *)
      let literal_value, signals = read_literal signals in
      let literal =
        Rdf.Literal.make literal_value (Rdf.Namespace.rdf "XMLLiteral")
      in

      let triple =
        Rdf.Triple.(make subject predicate (Object.of_literal literal))
      in
      emit_triple triple;

      (* handle reification *)
      reify ctx emit_triple attributes triple;

      (true, increment_list_expansion_counter, signals)
  (* 7.2.18 Production parseTypeResourcePropertyElt *)
  | Seq.Cons (`El_start (name, attributes), signals)
    when get_rdf_parse_type attributes = Some "Resource"
         && valid_parse_type_property_element_attributes attributes ->
      let predicate, increment_list_expansion_counter =
        if name = rdf "li" then
          ( Rdf.Namespace.rdf ("_" ^ Int.to_string ctx.list_expansion_counter)
            |> Rdf.Triple.Predicate.of_iri,
            true )
        else (iri_of_name name |> Rdf.Triple.Predicate.of_iri, false)
      in
      let n = Rdf.Blank_node.generate ~seed:ctx.seed () in

      let triple =
        Rdf.Triple.(make subject predicate (Object.of_blank_node n))
      in

      (* emit triple connecting parent subject to resource *)
      emit_triple triple;

      (* handle reification *)
      reify ctx emit_triple attributes triple;

      (* parse all child property elements *)
      let signals =
        parse_property_elements ctx emit_triple
          (Rdf.Triple.Subject.of_blank_node n)
          signals
      in

      (true, increment_list_expansion_counter, signals)
  (* 7.2.19 Production parseTypeCollectionPropertyElt *)
  | Seq.Cons (`El_start (name, attributes), signals)
    when get_rdf_parse_type attributes = Some "Collection"
         && valid_parse_type_property_element_attributes attributes ->
      let rec parse_node_elements ctx signals subjects =
        match parse_node_element ctx emit_triple signals with
        | Some s, ctx, signals -> parse_node_elements ctx signals (s :: subjects)
        | None, _ctx, signals -> (signals, List.rev subjects)
      in
      let signals, collection_subjects = parse_node_elements ctx signals [] in

      let rec emit_collection subject predicate elements =
        match elements with
        | head :: rest ->
            let bnode = Rdf.Blank_node.generate ~seed:ctx.seed () in
            (* triple connecting subject to fresh bnode *)
            let connecting_triple =
              Rdf.Triple.(make subject predicate (Object.of_blank_node bnode))
            in
            emit_triple connecting_triple;
            (* set value to head *)
            emit_triple
              Rdf.Triple.(
                make
                  (Subject.of_blank_node bnode)
                  (Predicate.of_iri @@ Rdf.Namespace.rdf "first")
                  (Object.of_term @@ Subject.to_term head));
            (* continue with rest *)
            connecting_triple
            :: emit_collection
                 (Rdf.Triple.Subject.of_blank_node bnode)
                 (Rdf.Triple.Predicate.of_iri @@ Rdf.Namespace.rdf "rest")
                 rest
        | [] ->
            let triple =
              Rdf.Triple.(
                make subject predicate (Object.of_iri @@ Rdf.Namespace.rdf "nil"))
            in
            emit_triple triple;
            [ triple ]
      in

      let collection_triples =
        emit_collection subject
          (Rdf.Triple.Predicate.of_iri @@ iri_of_name name)
          collection_subjects
      in

      reify ctx emit_triple attributes (List.hd collection_triples);

      (true, false, signals)
  (* 7.2.21 Production emptyPropertyElt *)
  | Seq.Cons (`El_start (name, attributes), signals)
    when is_property_element_uri name
         && lookahead_is_end signals
         && (Option.is_none @@ get_rdf_parse_type attributes) ->
      let attribute_names =
        NameSet.of_list
        @@ List.filter (fun name -> not @@ is_xmlns name)
        @@ List.map fst attributes
      in
      if
        NameSet.is_empty attribute_names
        || NameSet.equal attribute_names (NameSet.singleton (rdf "ID"))
      then (
        let predicate, increment_list_expansion_counter =
          if name = rdf "li" then
            ( Rdf.Namespace.rdf ("_" ^ Int.to_string ctx.list_expansion_counter)
              |> Rdf.Triple.Predicate.of_iri,
              true )
          else (iri_of_name name |> Rdf.Triple.Predicate.of_iri, false)
        in
        let triple =
          Rdf.Triple.(
            make subject predicate (Object.of_literal @@ string_literal ctx ""))
        in

        emit_triple triple;

        (* handle reification *)
        reify ctx emit_triple attributes triple;

        (true, increment_list_expansion_counter, read_end_element signals))
      else
        let n_subject = empty_property_element_subject ctx attributes in

        (* handle the property attributes *)
        node_handle_property_attributes ctx emit_triple n_subject attributes;

        let predicate, increment_list_expansion_counter =
          if name = rdf "li" then
            ( Rdf.Namespace.rdf ("_" ^ Int.to_string ctx.list_expansion_counter)
              |> Rdf.Triple.Predicate.of_iri,
              true )
          else (iri_of_name name |> Rdf.Triple.Predicate.of_iri, false)
        in

        (* emit the connecting triple *)
        let triple =
          Rdf.Triple.(
            make subject predicate (Object.of_term @@ Subject.to_term n_subject))
        in
        emit_triple triple;

        (* handle reification *)
        reify ctx emit_triple attributes triple;

        (* continue parsing *)
        (true, increment_list_expansion_counter, read_end_element signals)
  | Seq.Cons (`El_start (name, _), _) ->
      raise
      @@ Invalid_argument
           (Format.asprintf "got an invalid property element (%a)" Xmlm.pp_name
              name)
  | Seq.Cons (`Data _, _) ->
      raise
      @@ Invalid_argument
           "got a XML data signal when expecting a property element"
  (* Return signals up one layer *)
  | Seq.Cons (`El_end, signals) -> (false, false, signals)

and parse_property_elements ctx emit_triple subject signals =
  match parse_property_element ctx emit_triple subject signals with
  | true, increment_list_expansion_counter, signals ->
      let ctx =
        if increment_list_expansion_counter then
          { ctx with list_expansion_counter = ctx.list_expansion_counter + 1 }
        else ctx
      in
      parse_property_elements ctx emit_triple subject signals
  | false, _, signals -> signals

(* Parse a signle node element (nodeElement production) and return subject *)
and parse_node_element ctx emit_triple signals =
  match signals () with
  | Seq.Nil ->
      raise @@ Invalid_argument "signals ended while expecting node element"
  | Seq.Cons (`Dtd _, _) -> raise @@ Invalid_argument "unexpected DTD"
  | Seq.Cons (`El_start (name, attributes), signals)
    when is_valid_node_element_name name
         && valid_node_element_attributes attributes ->
      (* Set language *)
      let local_ctx = set_language ctx attributes in

      (* Set the base_iri *)
      let local_ctx = set_base local_ctx attributes in

      (* Compute the subject *)
      let local_ctx, subject =
        node_element_subject local_ctx (name, attributes)
      in

      (* Add a type statement if it is not a rdf:Description *)
      if name <> rdf "Description" then
        let object' = iri_of_name name |> Rdf.Triple.Object.of_iri in
        let type_triple =
          Rdf.Triple.(
            make subject (Predicate.of_iri @@ Rdf.Namespace.rdf "type") object')
        in
        emit_triple type_triple
      else ();

      (* Handle properties attributes *)
      node_handle_property_attributes local_ctx emit_triple subject attributes;

      (* parse property elements. This will also read the End_element
         of this node. *)
      let signals =
        parse_property_elements
          (* Initialize list expansion counter *)
          { local_ctx with list_expansion_counter = 1 }
          emit_triple subject signals
      in

      ( Some subject,
        (* scope the language and base iri *)
        { local_ctx with lang = ctx.lang; base_iri = ctx.base_iri },
        signals )
  | Seq.Cons (`El_start tag, _) ->
      raise
      @@ Invalid_argument
           (Format.asprintf "got invalid node element (%a)" Xmlm.pp_tag tag)
  | Seq.Cons (`Data _, _) ->
      raise
      @@ Invalid_argument "got a XML data signal when expecting a node element"
  (* return signals up one layer *)
  | Seq.Cons (`El_end, signals) -> (None, ctx, signals)

let rec parse_root ctx emit_triple signals =
  match signals () with
  | Seq.Nil -> signals
  (* ignore DTD *)
  | Seq.Cons (`Dtd _, signals) -> parse_root ctx emit_triple signals
  | Seq.Cons (`El_start (name, attributes), signals) when name = rdf "RDF" ->
      let ctx = set_base ctx attributes in
      let ctx = set_language ctx attributes in

      let rec parse_all_node_elements ctx signals =
        match parse_node_element ctx emit_triple signals with
        | None, _ctx, signals ->
            (* The end element for rdf:RDF was read in parse_node_element *)
            signals
        | Some _subject, ctx, signals ->
            (* parse_node_element parsed a single node element. continue with next. *)
            parse_all_node_elements ctx signals
      in
      parse_all_node_elements ctx signals
  | Seq.Cons (`El_start (name, _attributes), _signals) -> (
      match parse_node_element ctx emit_triple signals with
      | Some _subject, _ctx, signals -> signals
      | _ ->
          raise
          @@ Invalid_argument
               (Format.asprintf "Root element is not a valid node element (%a)."
                  Xmlm.pp_name name))
  | Seq.Cons (`Data _tag, _signals) ->
      raise
      @@ Invalid_argument "got a XML data signal when expecting root element"
  | Seq.Cons (`El_end, _signals) ->
      raise
      @@ Invalid_argument
           "got a XML element end signal when expecting root element"

let parse ?base_iri ?(seed = Random.State.make_self_init ()) ~f signals =
  let _seq =
    parse_root
      { base_iri; lang = None; list_expansion_counter = 0; seed; used_ids = [] }
      f signals
  in

  ()

let parse_to_graph ?base_iri ?seed signals =
  let graph = ref Rdf.Graph.empty in
  parse ?base_iri ?seed
    ~f:(fun triple -> graph := Rdf.Graph.add triple !graph)
    signals;
  !graph

(* Input helpers *)

let rec persistent_seq seq =
  let node = ref None in
  fun () ->
    match !node with
    | Some node -> node
    | None -> (
        match seq () with
        | Seq.Nil ->
            node := Some Seq.Nil;
            Seq.Nil
        | Seq.Cons (value, rest) ->
            let p_node = Seq.Cons (value, persistent_seq rest) in
            node := Some p_node;
            p_node)

let xmlm_input_to_seq input =
  let rec read () =
    if Xmlm.eoi input then Seq.Nil else Seq.Cons (Xmlm.input input, read)
  in
  read |> persistent_seq

(* Serialization *)

let xmlns local = ("http://www.w3.org/2000/xmlns/", local)

module Encode = struct
  let prefix_regexps prefixes =
    ("rdf", Rdf.Namespace.rdf "") :: prefixes
    |> List.map (fun (_prefix, iri) ->
           let iri_s = Rdf.Iri.to_string iri in
           (Str.(regexp @@ "^" ^ quote iri_s), iri_s))

  let split_into_xml_qname iri =
    match Uri.fragment iri with
    | Some fragment ->
        (* if iri has fragment part, split at # *)
        let prefix = Uri.(to_string @@ with_fragment iri None) ^ "#" in
        ((prefix, fragment), Some prefix)
    | None ->
        let s = Rdf.Iri.to_string iri in
        let prefix = String.(sub s 0 (length s - 1)) in
        let suffix = String.(sub s (length s - 1) 1) in
        ((prefix, suffix), Some prefix)

  let iri_to_qname ~prefix_regexps iri =
    let iri_s = Rdf.Iri.to_string iri in
    List.find_map
      (fun (re, iri) ->
        match Str.full_split re iri_s with
        | [ Str.Delim _; Str.Text suffix ] -> Some (iri, suffix)
        | _ -> None)
      prefix_regexps
    |> Option.map (fun name -> (name, None))
    |> Option.value ~default:(split_into_xml_qname iri)

  let encode_predicate_object ~prefix_regexps predicate object' =
    let predicate_name, ns0_prefix_opt =
      Rdf.Triple.Predicate.map (iri_to_qname ~prefix_regexps) predicate
    in

    let ns0 =
      match ns0_prefix_opt with
      | Some prefix -> [ (xmlns "ns0", prefix) ]
      | None -> []
    in

    Rdf.Triple.Object.map
      (fun object_iri ->
        List.to_seq
          [
            `El_start
              ( predicate_name,
                [ (rdf "resource", Rdf.Iri.to_string object_iri) ] @ ns0 );
            `El_end;
          ])
      (fun object_bnode ->
        List.to_seq
          [
            `El_start
              ( predicate_name,
                [ (rdf "nodeID", Rdf.Blank_node.identifier object_bnode) ] @ ns0
              );
            `El_end;
          ])
      (fun object_literal ->
        match Rdf.Literal.language object_literal with
        | Some lang ->
            List.to_seq
              [
                `El_start
                  ( predicate_name,
                    [ (("http://www.w3.org/XML/1998/namespace", "lang"), lang) ]
                    @ ns0 );
                `Data (Rdf.Literal.canonical object_literal);
                `El_end;
              ]
        | None ->
            let datatype = Rdf.Literal.datatype object_literal in
            if Rdf.Iri.equal datatype (Rdf.Namespace.xsd "string") then
              List.to_seq
                [
                  `El_start (predicate_name, [] @ ns0);
                  `Data (Rdf.Literal.canonical object_literal);
                  `El_end;
                ]
            else
              List.to_seq
                [
                  `El_start
                    ( predicate_name,
                      [ (rdf "datatype", Rdf.Iri.to_string datatype) ] @ ns0 );
                  `Data (Rdf.Literal.canonical object_literal);
                  `El_end;
                ])
      object'

  let encode_description ~prefix_regexps (subject, predicate_objects) =
    let start_description_element =
      Rdf.Triple.Subject.map
        (fun subject_iri ->
          `El_start
            (rdf "Description", [ (rdf "about", Rdf.Iri.to_string subject_iri) ]))
        (fun subject_bnode ->
          `El_start
            ( rdf "Description",
              [ (rdf "nodeID", Rdf.Blank_node.identifier subject_bnode) ] ))
        subject
    in
    (* start with the start element for the description *)
    Seq.cons start_description_element
    (* handle all predicate elements *)
    @@ Seq.flat_map
         (fun (predicate, objects) ->
           Seq.flat_map
             (encode_predicate_object ~prefix_regexps predicate)
             objects)
         predicate_objects
    (* append descripton element end *)
    |> fun s -> Seq.append s (Seq.return `El_end)
end

let to_signals ?(prefixes = []) graph =
  let prefix_regexps = Encode.prefix_regexps prefixes in
  let start_rdf_element =
    `El_start
      ( rdf "RDF",
        [ (xmlns "rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#") ]
        @ (prefixes
          |> List.map (fun (prefix, iri) ->
                 (xmlns prefix, Rdf.Iri.to_string iri))) )
  in
  Seq.cons start_rdf_element
  @@ Seq.flat_map
       (Encode.encode_description ~prefix_regexps)
       (Rdf.Graph.to_nested_seq graph)
  (* append rdf element end *)
  |> fun s -> Seq.append s (Seq.return `El_end)

let signals_of_description ?(prefixes = []) description =
  let prefix_regexps = Encode.prefix_regexps prefixes in
  let subject = Rdf.Description.subject description in
  let nested_seq = Rdf.Description.to_nested_seq description in
  Encode.encode_description ~prefix_regexps (subject, nested_seq)
