(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 * SPDX-FileCopyrightText: 2022 alleycat <arie@alleycat.cc>
 * SPDX-FileCopyrightText: 2012-2021 Institut National de Recherche en Informatique et en Automatique
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This file contains parts copied from the files `ttl_lex.ml`, `ttl_types.ml` and `ttl.ml` of the GPL-3.0 licensed ocaml-rdf library by Zoggy (https://framagit.org/zoggy/ocaml-rdf).
 *
 *)

(* infix operator for Seq.append, saves quite some lines *)
(* TODO append doesn't work well with js_of_ocaml, and is
 * not tail-recursive *)
let (@@@) s t = Seq.append s t

module Turtle_types = struct
  (* Here, we define the type `statement`, which is a `turtle statement`. A turtleDoc,
   * which is a list of statements, is the entry point of the turtle grammar. We don't
   * need to define the type turtleDoc because when parsing a turtle document,
   * we do this statement by statement, keeping a context and emitting triples on the go. *)

  (* The pp functions are quite rough, and are provided to use
   * for debugging *)

  module Ordered_string = struct
    type t = string

    let compare = String.compare
  end

  module SMap = Map.Make (Ordered_string)

  type context = {
    base_iri : Rdf.Iri.t;
    prefixes : Rdf.Iri.t SMap.t;
    blanks : Rdf.Blank_node.t SMap.t;
    seed : Random.State.t;
  }

  type iriref = string
  type directive = Prefix of string * iriref | Base of iriref
  type qname = string option * string option
  type iri = Iriref of iriref | Qname of qname
  type language = string
  type literal = String of (string * language option * iri option)
  type pred = Pred_iri of iri | Pred_a

  type object_ = Obj_iri of iri | Obj_blank of blank | Obj_literal of literal
  and blank =
    | NodeId of string
    | Empty
    | PredObjs of predobj list
    | Collection of object_ list
  and predobj = pred * object_ list

  type subject = Sub_iri of iri | Sub_blank of blank
  type statement = Directive of directive | Triples of subject * predobj list


  let is_long_quote_escape = function
    | '"' | '\\' -> true
    | _ -> false

  let is_reserved_char = function
    | '~' | '.' | '-' | '!' | '$' | '&' | '\'' | '(' | ')' | '*' | '+' | ','
    | ';' | '=' | '/' | '?' | '#' | '@' | '%' | '_' ->
        true
    | _ -> false

  (* maybe not tail-recursive *)
  let escaped matcher s =
    String.fold_left (fun acc c ->
      let c' = String.make 1 c in
      acc ^ if matcher c then "\\" ^ c' else c'
    ) "" s
  let escape_reserved_char = escaped is_reserved_char
  let escape_long_quote = escaped is_long_quote_escape

  (* @TODO check whether it is appropriate to use List.equal for checking
   * certain equalities. It might be that it is more natural to use the notion
   * of equality for sets. We could e.g. implement a function list_equal_up_to_order. *)
  let equal_iriref a b = String.equal a b
  let equal_directive a b = match a, b with
    | Prefix (a1, a2), Prefix (b1, b2) ->
      String.equal a1 b1 && equal_iriref a2 b2
    | Base a, Base b -> equal_iriref a b
    | _-> false
  let equal_qname a b = match a, b with
    | (a1, a2), (b1,b2) ->
      Option.equal String.equal a1 b1
      &&
      Option.equal String.equal a2 b2
  let equal_iri a b = match a, b with
    | Iriref a, Iriref b -> equal_iriref a b
    | Qname a, Qname b -> equal_qname a b
    | _-> false
  let equal_language a b = String.equal a b
  let equal_literal a b = match a, b with
    | String (a1, a2, a3), String (b1, b2, b3) ->
      String.equal a1 b1
      &&
      Option.equal equal_language a2 b2
      &&
      Option.equal equal_iri a3 b3
  and equal_pred a b = match a, b with
    | Pred_iri a, Pred_iri b -> equal_iri a b
    | Pred_a, Pred_a -> true
    | _ -> false
  let rec equal_object a b = match a, b with
    | Obj_iri a, Obj_iri b -> equal_iri a b
    | Obj_blank a, Obj_blank b -> equal_blank a b
    | Obj_literal a, Obj_literal b -> equal_literal a b
    | _ -> false
  and equal_blank a b = match a, b with
    | NodeId a, NodeId b -> String.equal a b
    | Empty, Empty -> true
    | PredObjs a, PredObjs b -> List.equal equal_predobj a b
    | Collection a, Collection b -> List.equal equal_object a b
    | _ -> false
  and equal_predobj a b = match a, b with
    | (a1, a2), (b1, b2) ->
      equal_pred a1 b1
      &&
      List.equal equal_object a2 b2
  let equal_subject a b = match a, b with
    | Sub_iri a, Sub_iri b -> equal_iri a b
    | Sub_blank a, Sub_blank b -> equal_blank a b
    | _ -> false
  let _equal_statement a b = match a, b with
    | Directive a, Directive b -> equal_directive a b
    | Triples (a1, a2), Triples (b1, b2) ->
      equal_subject a1 b1
      &&
      List.equal equal_predobj a2 b2
    | _ -> false


  let pp_iriref ppf = function s -> Fmt.pf ppf "<%s>" s

  let pp_directive ppf = function
    | Prefix (s, iri) -> Fmt.pf ppf "@prefix %s:%s ." s iri
    | Base s -> Fmt.pf ppf "@base %s: ." s

  let pp_qname ppf = function
    | s, t ->
        Fmt.pf ppf "%a:%a"
          (Fmt.option ~none:(Fmt.any "") Fmt.string)
          s
          (Fmt.option ~none:(Fmt.any "") Fmt.string)
          t

  let pp_iri ppf = function
    | Iriref iriref -> Fmt.pf ppf "%a" pp_iriref iriref
    | Qname qname -> Fmt.pf ppf "%a" pp_qname qname

  let pp_language ppf s = Fmt.pf ppf "@%s" s

  let pp_literal ppf = function
    | String (s, lo, io) ->
        Fmt.pf ppf "%s%a^^%a" s
          (Fmt.option ~none:(Fmt.any "") pp_language)
          lo
          (Fmt.option ~none:(Fmt.any "") pp_iri)
          io

  let pp_pred ppf = function
    | Pred_iri iri -> Fmt.pf ppf "%a" pp_iri iri
    | Pred_a -> Fmt.pf ppf "a"

  let rec pp_object_ ppf = function
    | Obj_iri i -> Fmt.pf ppf "%a" pp_iri i
    | Obj_blank b -> Fmt.pf ppf "%a" pp_blank b
    | Obj_literal l -> Fmt.pf ppf "%a" pp_literal l

  and pp_blank ppf = function
    | NodeId s -> Fmt.pf ppf "_:%s" s
    | Empty -> Fmt.pf ppf ""
    | PredObjs predobjs ->
        Fmt.pf ppf "%a" (Fmt.list ~sep:(Fmt.any ";") pp_predobj) predobjs
    | Collection objs ->
        Fmt.pf ppf "%a" (Fmt.list ~sep:(Fmt.any " ") pp_object_) objs

  and pp_predobj ppf = function
    | pred, objs ->
        Fmt.pf ppf "%a %a" pp_pred pred
          (Fmt.list ~sep:(Fmt.any ",") pp_object_)
          objs

  and pp_subject ppf = function
    | Sub_iri iri -> Fmt.pf ppf "%a" pp_iri iri
    | Sub_blank blank -> Fmt.pf ppf "%a" pp_blank blank

  let _pp_statement ppf = function
    | Directive directive -> Fmt.pf ppf "%a @." pp_directive directive
    | Triples (subject, predobjs) ->
        Fmt.pf ppf "%a %a . @." pp_subject subject
          (Fmt.list ~sep:(Fmt.any ";") pp_predobj)
          predobjs

end

module Encode = struct

  (* it doesn'n seem to cause conflicts to open both of these *)
  open Turtle_types
  open Rdf

  (* Functions to transform a description into a turtle statement *)

  (* TODO remove:
   * short description of what needs to happen:
   * input: list of (string, iri)
   * also: some description to encode;
   * then we want to replace (parts of) all the iri's in
   * the description with the corresponding strings:
   * e.g.
   * [("p", <http://a.example/s>)] = list
   * <http://example/s> <http://example/p> <http://example/o>
   * --->
   * p: <http://example/p> <http://example/o>
   * OR
   * @prefix p: <http://a.example/>.
   * <http://a.example/s> <http://a.example/p> p:o . *)

  let prefix_regexps prefixes =
    (* ("rdf", Namespace.rdf "") :: *)
    prefixes
    |> List.map (fun (prefix, iri) ->
        let iri_s = Iri.to_string iri in
        (* the ^ matches at the beginning of a line *)
        (prefix, Str.(regexp @@ "^" ^ quote iri_s)))

  let turtle_iri ~base ~prefix_regexps iri =
    let iri_s = Iri.to_string iri in

    (* remove the base from the iri if it's present *)
    let iri =
      let re = Str.(regexp @@ "^" ^ quote (Iri.to_string base)) in
      match Str.full_split re iri_s with
      | [ Str.Delim _; Str.Text suffix ] -> suffix
      | _ -> iri_s in

    List.find_map
      (fun (prefix, re) ->
        match Str.full_split re iri_s with
        | [ Str.Delim _; Str.Text suffix ] -> Some (Qname (Some prefix, Some suffix))
        | _ -> None)
      prefix_regexps
    |> Option.value ~default:(Iriref iri)

  let turtle_pred ~base ~prefix_regexps pred =
    Rdf.Triple.Predicate.map
      (fun iri -> Pred_iri (turtle_iri ~base ~prefix_regexps iri))
      pred

  let turtle_object ~base ~prefix_regexps obj =
    Triple.Object.map
      (fun iri -> Obj_iri (turtle_iri ~base ~prefix_regexps iri))
      (fun bnode -> Obj_blank (NodeId (Blank_node.identifier bnode)))
      (fun lit ->
         Obj_literal (
           String (
             Literal.canonical lit,
             Literal.language lit,
             Some (turtle_iri ~base ~prefix_regexps (Literal.datatype lit)))))
      obj

  let turtle_subject ~base ~prefix_regexps sub =
    Triple.Subject.map
      (fun iri -> Sub_iri (turtle_iri ~base ~prefix_regexps iri))
      (fun bnode -> Sub_blank (NodeId (Blank_node.identifier bnode)))
      sub

  let add_objects ~base ~prefix_regexps pred obj_seq triple =
    let obj_list =
      obj_seq |> List.of_seq |> List.map (turtle_object ~base ~prefix_regexps) in
    let pred = turtle_pred ~base ~prefix_regexps pred in
    match triple with
    | Triples (subject, predobjs) ->
      Triples (subject, (pred, obj_list) :: predobjs)
    | Directive _ ->
      failwith "This should not be possible, we're transforming a
        description into a turtle triple"

  let statement_of_description ~prefixes ~base desc =
    let prefix_regexps = prefix_regexps prefixes in
    let sub =
      desc |> Description.subject |> turtle_subject ~base ~prefix_regexps in
    (* we create a turtle triple with only the subject given.
     * This is a bit ad hoc *)
    let sub_triple = Triples (sub , []) in
    let nested_seq = Rdf.Description.to_nested_seq desc in
    Seq.fold_left
      (fun acc (pred, obj_seq) ->
         add_objects ~base ~prefix_regexps pred obj_seq acc)
      sub_triple nested_seq

  let statement_of_prefix (s, iri) =
    Directive (Prefix (s, Iri.to_string iri))

  let statement_of_base iri =
    Directive (Base (Iri.to_string iri))

  let encode_list sep encoder = function
    | [] -> ""
    | [x] -> encoder x
    | x :: xs -> encoder x ^ List.fold_left (
        fun acc x -> acc ^ sep ^ (encoder x)
      ) "" xs
  let encode_iriref iriref  = "<" ^ iriref ^ ">"
  let encode_directive = function
    | Prefix (s, iri) -> "@prefix " ^ s ^ ":" ^ encode_iriref iri
    | Base s -> "@base " ^ "<" ^ s ^ ">"
  let encode_qname = function
    | s, t ->
      let s = Option.value s ~default:"" in
      let t = Option.value t ~default:"" in
      s ^ ":" ^ escape_reserved_char t
  let encode_iri = function
    | Iriref iriref -> encode_iriref iriref
    | Qname qname -> encode_qname qname
  let encode_language language = "@" ^ language
  (* we encode all literals with long quotes.
   * If a language is present, we encode it,
   * and forget about a possible datatype.
   * TODO figure out why there can be a datatype
   * when there is a lang. It should not really be possible..
   * If the datatype is not empty, we add the iri
   * with fragment = datatype. *)
  let encode_literal = fun
    (String (s, lo, io)) ->
    let quote x =
      let qu = "\"\"\"" in
      qu ^ x ^ qu in
    match lo with
    | Some lang ->
      quote (escape_long_quote s) ^ encode_language lang
    | None -> (
        match io with
        | Some iri ->
          quote (escape_long_quote s) ^ "^^" ^ encode_iri iri
        | None -> quote (escape_long_quote s))
  let encode_pred = function
    | Pred_iri iri -> encode_iri iri
    | Pred_a -> "a"
  let rec encode_object = function
    | Obj_iri i -> encode_iri i
    | Obj_blank b -> encode_blank b
    | Obj_literal l -> encode_literal l
  and encode_blank = function
    | NodeId s -> "_:" ^ s
    | Empty -> "[]"
    | PredObjs predobjs ->
      "[" ^ encode_list " ;\n    " encode_predobj predobjs ^ "]"
    | Collection objs ->
      "(" ^ encode_list " " encode_object objs  ^ ")"
  and encode_predobj = function
    | pred, objs ->
      encode_pred pred
      ^ " "
      ^ encode_list " ,\n         "  encode_object objs
  and encode_subject= function
    | Sub_iri iri -> encode_iri iri
    | Sub_blank blank -> encode_blank blank
  let encode_statement = function
    | Directive directive -> encode_directive directive ^ " .\n"
    | Triples (subject, predobjs) ->
      Fmt.str "\n%s\n    %s .\n"
        (encode_subject subject)
        (encode_list " ;\n    " encode_predobj predobjs)

end

module Lexer = struct
  (* This module contains the Lexer for turtle documents. It's `main` function
   * has input `lexbuf` and output `token option`. Applied recursively,
   * this generates a `token option Seq.t`, which can be parsed using
   * a parser combinator, defined in the module Parser.*)

  (* We start with a few helper functions, in order to transform
   * strings like "abc\u0064" into "abcd".
   * See https://www.w3.org/TR/turtle/#sec-escapes for details *)

  module L = Sedlexing.Utf8

  let is_string_escaped_char = function
    | 'b' | 'f' | 'n' | 'r' | 't' | '"' | '\'' | '\\' -> true
    | _ -> false

  let string_escaped_chars_map = function
    | 'b' -> '\b'
    | 'f' -> Char.chr 0x0c
    | 'n' -> '\n'
    | 'r' -> '\r'
    | 't' -> '\t'
    | '"' -> '"'
    | '\'' -> '\''
    | '\\' -> '\\'
    | c -> c

  let is_escaped_uchar uc =
    try uc |> Uchar.to_char |> is_string_escaped_char with _ -> false

  (* here uc is an escaped character, so it's ok to use Uchar.to_char *)
  let escaped_uchars_map uc =
    uc |> Uchar.to_char |> string_escaped_chars_map |> Uchar.of_char

  let is_reserved_uchar uc =
    try uc |> Uchar.to_char |> Turtle_types.is_reserved_char with _ -> false

  (* Rather contrived function, with the accumulator keeping track of
   * number of Uchar's, and an Array to store Uchar's, while folding over
   * the string. Using e.g. a list or a sequence instead of an Array, would
   * either use List.append, or Seq.append, which would be very inefficient
   * and creates Stackoverflows in js_of_caml, or use a List.rev; which works
   * in js_of_ocaml, but it not very efficient either. *)
  let decode s =
    let len = String.length s in
    let b = Array.make len (Uchar.of_char '0') in
    let len' = Uutf.String.fold_utf_8
        (fun acc  _pos v ->
           match v with
           | `Malformed s ->
             raise
             @@ Invalid_argument
               (Format.sprintf
                  "Malformed UTF-8 while checking rdf:ID value (%s)" s)
           | `Uchar uchar -> Array.set b acc uchar; acc + 1)
        0 s in
    Array.sub b 0 len' |> Array.to_seq

  (* we assume that we have ASCII chars here. No problem if we fail *)
  let int_of_chars seq len =
    (* take first len uchars, and turn into a string *)
    let s =
      Seq.fold_left
        (fun s c -> s ^ String.make 1 (Uchar.to_char c))
        "" (Seq.take len seq)
    in
    let s2 = "0x" ^ s in
    try int_of_string s2
    with _ -> raise @@ Invalid_argument ("Invalid UTF8 code: " ^ s)

  (* we use this function after we already encountered a '\' *)
  let match_on_escape_numeric seq b =
    let len = Seq.length seq in
    match seq () with
    (* match on u and ensure there are at least 4 more chars *)
    | Seq.Cons (i, tail) when Uchar.of_char 'u' = i && len > 4 ->
        let d = int_of_chars tail 4 |> Uchar.of_int in
        let tail' = Seq.drop 4 tail in
        let () = Buffer.add_utf_8_uchar b d in
        tail'
    (* match on U and ensure there are at least 8 more chars *)
    | Seq.Cons (i, tail) when Uchar.of_char 'U' = i && len > 8 ->
        let d = int_of_chars tail 8 |> Uchar.of_int in
        let tail' = Seq.drop 8 tail in
        let () = Buffer.add_utf_8_uchar b d in
        tail'
    (* A '\' is only allowed if it is followed by a numeric sequence *)
    | Seq.Cons (i, _tail) ->
        raise
        @@ Invalid_argument
             (Fmt.str "unexpected char: %c after \\" (Uchar.to_char i))
    | Seq.Nil -> raise @@ Invalid_argument "unexpected: no char after \\"

  (* we use this function after we already encountered a '\' *)
  let match_on_escape_numeric_and_string seq b =
    let len = Seq.length seq in
    match seq () with
    (* match on escaped characters *)
    | Seq.Cons (i, tail) when is_escaped_uchar i ->
        let d = escaped_uchars_map i in
        let () = Buffer.add_utf_8_uchar b d in
        tail
    (* match on u and ensure there are at least 4 more chars *)
    | Seq.Cons (i, tail) when Uchar.of_char 'u' = i && len > 4 ->
        let d = int_of_chars tail 4 |> Uchar.of_int in
        let tail' = Seq.drop 4 tail in
        let () = Buffer.add_utf_8_uchar b d in
        tail'
    (* match on U and ensure there are at least 8 more chars *)
    | Seq.Cons (i, tail) when Uchar.of_char 'U' = i && len > 8 ->
        let d = int_of_chars tail 8 |> Uchar.of_int in
        let tail' = Seq.drop 8 tail in
        let () = Buffer.add_utf_8_uchar b d in
        tail'
    (* A '\\' is only allowed if it is followed by a numeric sequence or
     * a string escape *)
    | Seq.Cons (i, tail) ->
        let () = Buffer.add_utf_8_uchar b i in
        tail
    | Seq.Nil -> raise @@ Invalid_argument "unexpected: no char after \\"

  (* we use this function after we already encountered a '\' *)
  let match_on_escape_reserverd seq b =
    match seq () with
    (* match on reserverd characters *)
    | Seq.Cons (i, tail) when is_reserved_uchar i ->
        let () = Buffer.add_utf_8_uchar b i in
        tail
    (* A '\' is only allowed if it is followed by a reserved character *)
    | Seq.Cons (i, _tail) ->
        raise
        @@ Invalid_argument
             (Fmt.str "unexpected char: %c after \\" (Uchar.to_char i))
    | Seq.Nil ->
        raise @@ Invalid_argument (Fmt.str "unexpected: no char after \\")

  let unescape match_on_escape s =
    let rec iter seq b =
      match seq () with
      (* match on \ *)
      | Seq.Cons (i, tail) when Uchar.of_char '\\' = i ->
          let tail' = match_on_escape tail b in
          iter tail' b
      (* anything else, just add to buffer and continue *)
      | Seq.Cons (i, tail) ->
          let () = Buffer.add_utf_8_uchar b i in
          iter tail b
      | Seq.Nil -> Seq.empty
    in
    let len = String.length s in
    let b = Buffer.create len in
    let seq = decode s in
    let _ = iter seq b () in
    Buffer.contents b

  let unescape_numeric_and_string = unescape match_on_escape_numeric_and_string
  let unescape_numeric = unescape match_on_escape_numeric
  let unescape_reserved = unescape match_on_escape_reserverd

  (* Now the actual lexer starts *)
  let digit = [%sedlex.regexp? '0' .. '9']
  let alpha = [%sedlex.regexp? 'a' .. 'z' | 'A' .. 'Z']
  let hex = [%sedlex.regexp? digit | 'A' .. 'F' | 'a' .. 'f']

  let pn_chars_base =
    [%sedlex.regexp?
      ( alpha
      | 0x00C0 .. 0x00D6
      | 0x00D8 .. 0x00F6
      | 0x00F8 .. 0x02FF
      | 0x0370 .. 0x037D
      | 0x037F .. 0x1FFF
      | 0x200C .. 0x200D
      | 0x2070 .. 0x218F
      | 0x2C00 .. 0x2FEF
      | 0x3001 .. 0xD7FF
      | 0xF900 .. 0xFDCF
      | 0xFDF0 .. 0xFFFD
      | 0x10000 .. 0xEFFFF )]

  let pn_chars_u = [%sedlex.regexp? pn_chars_base | '_']

  let pn_chars =
    [%sedlex.regexp?
      pn_chars_u | '-' | digit | 0x00B7 | 0x0300 .. 0x036F | 0x203F .. 0x2040]

  let pn_prefix =
    [%sedlex.regexp? pn_chars_base, Opt (Star (pn_chars | '.'), pn_chars)]

  let percent = [%sedlex.regexp? '%', hex, hex]

  let pn_local_sec =
    [%sedlex.regexp?
      ( '\\',
        ( '_' | '~' | '.' | '-' | '!' | '$' | '&' | "'" | '(' | ')' | '*' | '+'
        | ',' | ';' | '=' | '/' | '?' | '#' | '@' | '%' ) )]

  let plx = [%sedlex.regexp? percent | pn_local_sec]

  let pn_local =
    [%sedlex.regexp?
      ( (pn_chars_u | ':' | digit | plx),
        Opt (Star (pn_chars | '.' | ':' | plx), (pn_chars | ':' | plx)) )]

  let uchar =
    [%sedlex.regexp?
      "\\u", hex, hex, hex, hex | "\\U", hex, hex, hex, hex, hex, hex, hex, hex]

  let iriref =
    [%sedlex.regexp?
      '<', Star (Compl (0x00 .. 0x20 | Chars "<>\\\"{}|^`") | uchar), '>']

  let pname_ns = [%sedlex.regexp? Opt pn_prefix, ':']
  let pname_ln = [%sedlex.regexp? pname_ns, pn_local]

  let blank_node_label =
    [%sedlex.regexp?
      "_:", (pn_chars_u | digit), Opt (Star (pn_chars | '.'), pn_chars)]

  let langtag =
    [%sedlex.regexp? '@', Plus alpha, Star ('-', Plus (alpha | digit))]

  let integer = [%sedlex.regexp? Opt ('+' | '-'), Plus digit]
  let decimal = [%sedlex.regexp? Opt ('+' | '-'), Star digit, '.', Plus digit]
  let exponent = [%sedlex.regexp? ('e' | 'E'), Opt ('+' | '-'), Plus digit]

  let double =
    [%sedlex.regexp?
      ( Opt ('+' | '-'),
        ( Plus digit, '.', Star digit, exponent
        | '.', Plus digit, exponent
        | Plus digit, exponent ) )]

  let echar =
    [%sedlex.regexp? '\\', ('t' | 'b' | 'n' | 'r' | 'f' | '\\' | '"' | '\\')]

  let string_literal_quote =
    [%sedlex.regexp?
      '"', Star (Compl (0x22 | 0x5C | 0xA | 0xD) | echar | uchar), '"']

  let string_literal_single_quote =
    [%sedlex.regexp?
      "'", Star (Compl (0x27 | 0x5C | 0xA | 0xD) | echar | uchar), "'"]

  let string_literal_long_single_quote =
    [%sedlex.regexp?
      "'''", Star (Opt ("'" | "''"), Compl ('\'' | '\\') | echar | uchar), "'''"]

  let string_literal_long_quote =
    [%sedlex.regexp?
      ( "\"\"\"",
        Star (Opt ('"' | "\"\""), Compl ('"' | '\\') | echar | uchar),
        "\"\"\"" )]

  let ws = [%sedlex.regexp? 0x20 | 0x9 | 0xD | 0xA]
  let anon = [%sedlex.regexp? '[', Star ws, ']']
  let comment = [%sedlex.regexp? '#', Star (Compl (0xA | 0xD))]
  let boolean = [%sedlex.regexp? "true" | "false"]

  let sparql_base =
    [%sedlex.regexp? ('b' | 'B'), ('a' | 'A'), ('s' | 'S'), ('e' | 'E')]

  let sparql_prefix =
    [%sedlex.regexp?
      ( ('p' | 'P'),
        ('r' | 'R'),
        ('e' | 'E'),
        ('f' | 'F'),
        ('i' | 'I'),
        ('x' | 'X') )]

  type token =
    | SEMICOLON
    | COMMA
    | DOT
    | HATHAT
    | AT_PREFIX
    | AT_BASE
    | PREFIX
    | BASE
    | A
    | ANON
    | LEFT_PAR
    | RIGHT_PAR
    | LEFT_BRACKET
    | RIGHT_BRACKET
    | Iriref_ of string
    | Identifier of string
    | At_identifier of string
    | Qname_ of string option * string option
    | Bname of string
    | String_ of string
    | Integer of string
    | Decimal of string
    | Double of string
    | Boolean of string

  let rec main lexbuf =
    match%sedlex lexbuf with
    | 'a' -> Some A
    | "\r\n" -> main lexbuf
    | '\r' -> main lexbuf
    | '\n' -> main lexbuf
    | comment -> main lexbuf
    | ws -> main lexbuf
    | anon -> Some ANON
    | '(' -> Some LEFT_PAR
    | ')' -> Some RIGHT_PAR
    | '[' -> Some LEFT_BRACKET
    | ']' -> Some RIGHT_BRACKET
    | ',' -> Some COMMA
    | ';' -> Some SEMICOLON
    | '.' -> Some DOT
    | "@prefix" -> Some AT_PREFIX
    | "@base" -> Some AT_BASE
    | sparql_prefix -> Some PREFIX
    | sparql_base -> Some BASE
    | langtag ->
        let s = L.lexeme lexbuf in
        let s = String.sub s 1 (String.length s - 1) in
        Some (At_identifier s)
    | "^^" -> Some HATHAT
    | boolean ->
        let s = L.lexeme lexbuf in
        Some (Boolean s)
    | integer ->
        let s = L.lexeme lexbuf in
        Some (Integer s)
    | decimal ->
        let s = L.lexeme lexbuf in
        Some (Decimal s)
    | double ->
        let s = L.lexeme lexbuf in
        Some (Double s)
    | iriref ->
        let s = L.lexeme lexbuf in
        let iri = String.sub s 1 (String.length s - 2) in
        let iri = unescape_numeric iri in
        Some (Iriref_ iri)
    | pname_ns ->
        let s = L.lexeme lexbuf in
        Some (Identifier (String.sub s 0 (String.length s - 1)))
    | blank_node_label ->
        let s = L.lexeme lexbuf in
        let id = String.sub s 2 (String.length s - 2) in
        Some (Bname id)
    | pname_ln ->
        let s = L.lexeme lexbuf in
        let p = String.index s ':' in
        let s1 = match String.sub s 0 p with "" -> None | s -> Some s in
        let s2 =
          let len = String.length s in
          if len > p + 1 then
            String.sub s (p + 1) (len - p - 1)
            |> unescape_reserved |> Option.some
          else None
        in
        Some (Qname_ (s1, s2))
    | string_literal_quote | string_literal_single_quote ->
        let s = L.lexeme lexbuf in
        let s = String.sub s 1 (String.length s - 2) in
        Some (String_ (unescape_numeric_and_string s))
    | string_literal_long_quote | string_literal_long_single_quote ->
        let s = L.lexeme lexbuf in
        let s = String.sub s 3 (String.length s - 6) in
        Some (String_ (unescape_numeric_and_string s))
    | eof -> None
    | _ ->
        let s = L.lexeme lexbuf in
        raise @@ Invalid_argument (Fmt.str "Lexeme %s not handled" s)
end

module Turtle_to_rdf = struct
   (* See https://www.w3.org/TR/turtle/#sec-parsing for
    * more information about this module. The most important
    * function is `read`, which transforms a sequence of statements
    * into a sequence of triples *)

  open Rdf
  open Turtle_types

  (* @TODO move these to namespaces? *)
  let first_pred = Namespace.rdf "first" |> Triple.Predicate.of_iri
  let rest_pred = Namespace.rdf "rest" |> Triple.Predicate.of_iri
  let base_pred = Namespace.rdf "type" |> Triple.Predicate.of_iri
  let nil_obj = Namespace.rdf "nil" |> Triple.Object.of_iri

  (* @TODO what is the default scheme? Should it be in the context? *)
  let iri_of_iriref ctx s = Iri.resolve "" ctx.base_iri (Iri.of_string s)

  let iri_of_resource ctx = function
    | Iriref iri -> iri_of_iriref ctx iri
    | Qname (p, n) -> (
        let p = match p with None -> "" | Some s -> s in
        let base_iri =
          try SMap.find p ctx.prefixes
          with Not_found ->
            raise @@ Invalid_argument (Fmt.str "Unknown namespace %s" p)
        in
        match n with
        | None -> base_iri
        | Some n ->
            let iri = Iri.to_string base_iri ^ n in
            Iri.of_string iri)

  let new_blank_id ctx = Blank_node.generate ~seed:ctx.seed

  let get_blank_node ctx id =
    try (SMap.find id ctx.blanks, ctx)
    with Not_found ->
      let bid = new_blank_id ctx () in
      let ctx = { ctx with blanks = SMap.add id bid ctx.blanks } in
      (bid, ctx)

  let rec from_blank ctx b =
    match b with
    | NodeId id ->
      let node, ctx = get_blank_node ctx id in
      Term.of_blank_node node, ctx, Seq.empty
    | Empty ->
      let node = new_blank_id ctx () in
      Term.of_blank_node node, ctx, Seq.empty
    | PredObjs predobjs ->
      let node = new_blank_id ctx () in
      let sub = Triple.Subject.of_blank_node node in
      let ctx, seq =
        List.fold_left
          (from_objects sub)
          (ctx, Seq.empty) predobjs in
      Term.of_blank_node node, ctx, seq
    | Collection [] ->
      Term.of_iri @@ Namespace.rdf "nil", ctx, Seq.empty
    | Collection objs ->
      let node = new_blank_id ctx () in
      let ctx, seq = from_collection ctx node Seq.empty objs in
      Term.of_blank_node node, ctx, seq

  and from_collection ctx node seq objs =
      match objs with
      | [] -> assert false
      | h :: q -> (
          let obj, ctx, seq_node = from_object_ ctx h in
          let sub_bnode = Triple.Subject.of_blank_node node in
          let fst_triple = Triple.make sub_bnode first_pred obj in
          match q with
          | [] ->
            let lst_triple = Triple.make sub_bnode rest_pred nil_obj in
            let seq =
              Seq.return fst_triple @@@ seq_node
              @@@ seq @@@ Seq.return lst_triple in
            ctx, seq
          | _ ->
            let node = new_blank_id ctx () in
            let obj = Triple.Object.of_blank_node node in
            let triple = Triple.make sub_bnode rest_pred obj in
            let seq =
              Seq.return fst_triple @@@ seq_node @@@
              seq @@@ Seq.return triple in
            from_collection ctx node seq q)

  and from_object_ ctx obj =
      match obj with
      | Obj_iri res ->
        let triple = Triple.Object.of_iri @@ iri_of_resource ctx res in
        triple, ctx, Seq.empty
      | Obj_blank b ->
        let term, ctx, seq = from_blank ctx b in
        Triple.Object.of_term @@ term, ctx, seq
      | Obj_literal (String (str, lang_opt, iri_opt)) -> (
          match iri_opt with
          | Some r ->
            let iri = iri_of_resource ctx r in
            let triple =
              Triple.Object.of_literal @@ Literal.make str iri in
            triple, ctx, Seq.empty
          | None -> (
              match lang_opt with
             | Some l ->
               let triple =
                 Triple.Object.of_literal @@ Literal.make_string ~language:l str in
               triple, ctx, Seq.empty
             | None ->
               let triple =
                 Triple.Object.of_literal @@ Literal.make_string str in
               triple, ctx, Seq.empty))

  and from_object sub pred (ctx, seq) obj =
    let obj, ctx, node_seq = from_object_ ctx obj in
    ctx, seq @@@ node_seq @@@ Seq.return (Rdf.Triple.make sub pred obj)

  and from_objects sub (ctx, seq) (pred, objs) =
    let pred =
      match pred with
      | Pred_iri r -> Triple.Predicate.of_iri (iri_of_resource ctx r)
      | Pred_a -> base_pred
    in
    List.fold_left (from_object sub pred) (ctx, seq) objs

  and from_predobjs sub ctx predobjs =
    match sub with
    | Sub_iri r ->
      let sub = Triple.Subject.of_iri (iri_of_resource ctx r) in
      List.fold_left (from_objects sub) (ctx, Seq.empty) predobjs
    | Sub_blank b ->
        let term, ctx, blank_seq = from_blank ctx b in
        let f_iri iri = Triple.Subject.of_iri iri in
        let f_blank_node bnode = Triple.Subject.of_blank_node bnode in
        let f_literal lit =
          raise
          @@ Invalid_argument
               (Fmt.str "Unexpected literal: %a, this cannot be a subject."
                  Literal.pp lit)
        in
        let sub = Term.map f_iri f_blank_node f_literal term in
        let ctx, predobjs_seq = List.fold_left (from_objects sub) (ctx, Seq.empty) predobjs in
        ctx, blank_seq @@@ predobjs_seq

  let read ctx seq () =

    let read_statement ctx k seq =
      match Seq.uncons seq with
      | Some (Directive (Prefix (s, iri)), seq) ->
          let iri = iri_of_iriref ctx iri in
          let ctx = { ctx with prefixes = SMap.add s iri ctx.prefixes } in
          k ctx seq
      | Some (Directive (Base iri), seq) ->
          let iri = iri_of_iriref ctx iri in
          let ctx = { ctx with base_iri = iri } in
          k ctx seq
      | Some (Triples (sub, predobjs), seq) ->
          let ctx, predobjs_seq = from_predobjs sub ctx predobjs in
          predobjs_seq @@@ (k ctx seq)
      | None -> failwith "This should not be possible"
    in

    let rec read ctx seq () =
      match Seq.uncons seq with
      | Some (statement, seq) ->
          read_statement ctx read (Seq.cons statement seq) ()
      | None -> Seq.Nil
    in

    read ctx seq ()
end

module Parser = struct
  open Lexer
  open Turtle_types
  open Rdf

  (* This is a parser combinator that works on a sequence of tokens. First
   * we define a couple of standard functions, they can be used to parse
   * any sequence of tokens *)

  type token = Lexer.token
  (* type tokens = token Seq.t *)
  type 'a t = token Seq.t -> ('a * token Seq.t, string) Result.t

  (* let fail msg = Error msg *)
  let map p f seq =
    match p seq with Ok (v, rest) -> Ok (f v, rest) | Error msg -> Error msg

  let bind (p : 'a t) (f : 'a -> 'b t) seq =
    match p seq with Ok (v, rest) -> (f v) rest | Error msg -> Error msg

  let ( >>= ) p f = bind p f
  let ( >>| ) p f = map p f
  let ( *> ) a b = a >>= fun _ -> b

  let ( <* ) a b =
    a >>= fun x ->
    b >>| fun _ -> x

  let ( <|> ) p q seq =
    match p seq with Ok (v, rest) -> Ok (v, rest) | Error _ -> q seq

  let lift f p = map p f
  let lift2 f p1 p2 = lift f p1 >>= fun fv -> lift fv p2
  (* let lift3 f p1 p2 p3 = (lift2 f p1 p2) >>= (fun fvw -> lift fvw p3) *)

  let option' v p seq =
    match p seq with Ok (w, rest) -> Ok (w, rest) | Error _ -> Ok (v, seq)

  let token t t_name seq =
    match seq () with
    | Seq.Cons (x, rest) when x = t -> Ok (x, rest)
    | _ -> Error t_name

  (* note: this doesn't return an empty list when the input is empty *)
  let rec many p seq =
    match p seq with
    | Ok (x, rest) -> (
        match many p rest with
        | Ok (lst, rest') -> Ok (x :: lst, rest')
        | Error _ -> Ok ([ x ], rest))
    | Error _s -> Ok ([], seq)

  let cons x xs = x :: xs
  let many1 p = lift2 cons p (many p)

  let sep_by1 sep p =
    lift2 (fun x xs -> x :: xs) p (many (sep *> p)) <|> lift (fun x -> [ x ]) p

  (* The following functions are not used, but might be useful, e.g. to speed up *)
  (* let return v seq = Ok (v, seq) *)
  (* let fail msg = Error msg *)

  (* let any_token : token t = fun seq -> *)
  (* match seq () with *)
  (* | Seq.Cons (x, rest) -> Ok (x, rest) *)
  (* | _ -> Error ("expecting ") *)

  (* let peek_token : tokens -> token option * tokens = fun seq -> *)
  (* match seq () with *)
  (* | Seq.Nil -> None, Seq.empty *)
  (* | Seq.Cons (x, _rest) -> (Some x, seq) *)

  let eof seq =
    match seq () with
    | Seq.Nil -> Ok (None, Seq.empty)
    | _ -> Error "expecting eof"

  (* The rest of the functions are specific for parsing the sequence
   * of `turtle tokens`.*)

  let semicolon = token SEMICOLON "SemiColon"
  let comma = token COMMA "Comma"
  let dot = token DOT "dot"
  let hat_hat = token HATHAT "HatHat"
  let at_prefix = token AT_PREFIX "AtPrefix"
  let at_base = token AT_BASE "AtBase"
  let prefix = token PREFIX "Prefix"
  let base = token BASE "Base"
  let a = token A "A"
  let anon = token ANON "Anon"
  let left_par = token LEFT_PAR "LeftPar"
  let right_par = token RIGHT_PAR "RightPar"
  let left_bracket = token LEFT_BRACKET "LeftBracket"
  let right_bracket = token RIGHT_BRACKET "RightBracket"

  (* For reference, we provide some examples of what string can be
   * parsed to which type. E.g. rdf:humhum is parsed, by the
   * qname_ parser, to Qname_ (Some "rdf", Some "humhum") *)

  (* <iriref> *)
  let iriref seq =
    match seq () with
    | Seq.Cons (Iriref_ iri, rest) -> Ok (iri, rest)
    | _ -> Error "expecting Iriref"

  (* fjdl: OR : *)
  let identifier seq =
    match seq () with
    | Seq.Cons (Identifier s, rest) -> Ok (s, rest)
    | _ -> Error "expecting Identifier"

  (* @lang OR @lang-be *)
  let at_identifier seq =
    match seq () with
    | Seq.Cons (At_identifier s, rest) -> Ok (s, rest)
    | _ -> Error "expecting AtIdentifier"

  (* fdjk:fdjkfj OR :fjdkl *)
  let qname_ seq =
    match seq () with
    | Seq.Cons (Qname_ (s_opt, t_opt), rest) -> Ok ((s_opt, t_opt), rest)
    | _ -> Error "expecting Qname"

  (* _:fjdsk *)
  let bname seq =
    match seq () with
    | Seq.Cons (Bname s, rest) -> Ok (s, rest)
    | _ -> Error "expecting Bname"

  (* "some string" *)
  let string_ seq =
    match seq () with
    | Seq.Cons (String_ s, rest) -> Ok (s, rest)
    | _ -> Error "expecting String_"

  (* 12 *)
  let integer seq =
    match seq () with
    | Seq.Cons (Integer i, rest) -> Ok (i, rest)
    | _ -> Error "expecting Integer"

  (* 12.3 *)
  let decimal seq =
    match seq () with
    | Seq.Cons (Decimal d, rest) -> Ok (d, rest)
    | _ -> Error "expecting Decimal"

  let double seq =
    match seq () with
    | Seq.Cons (Double d, rest) -> Ok (d, rest)
    | _ -> Error "expecting Double"

  let boolean seq =
    match seq () with
    | Seq.Cons (Boolean b, rest) -> Ok (b, rest)
    | _ -> Error "expecting Boolean"

  (* : OR :fdjk OR fdjksl:fdjskl *)
  let prefixed_name =
    identifier
    >>| (fun p ->
          match p with "" -> Qname (None, None) | s -> Qname (Some s, None))
    <|> (qname_ >>| fun x -> Qname x)

  (* see prefixed_name OR <fdshj> *)
  let iri = lift (fun s -> Iriref s) iriref <|> prefixed_name

  (* @base <fdjskl> . OR BASE <hithere> *)
  let base' =
    lift (fun s -> Base s) (at_base *> iriref <* dot)
    <|> lift (fun iri -> Base iri) (base *> iriref)

  (* @prefix hi: <iriref> . OR PREFIX prefix: <iriref> *)
  let prefix_id =
    lift2
      (fun n iri -> Prefix (n, iri))
      (at_prefix *> identifier) (iriref <* dot)
    <|> lift2 (fun n iri -> Prefix (n, iri)) (prefix *> identifier) iriref

  (* @prefix hi: <iriref> . OR PREFIX pre: <irir> OR @base <hihi> *)
  let directive = prefix_id <|> base'

  (* ^^<iri> *)
  let datatype = hat_hat *> iri

  (* a OR <iri> *)
  let verb = lift (fun _ -> Pred_a) a <|> lift (fun iri -> Pred_iri iri) iri

  (* _:blankNODE OR [] *)
  let blanknode =
    lift (fun node -> NodeId node) bname <|> lift (fun _ -> Empty) anon

  (* "some string" *)
  (* "some string"@lang *)
  (* "some string"^^<datatype> *)
  (* 1698723 *)
  let literal =
    lift2 (fun s lang -> String (s, Some lang, None)) string_ at_identifier
    <|> lift2 (fun s dt -> String (s, None, Some dt)) string_ datatype
    <|> lift
          (fun i ->
            String
              ( i,
                None,
                Some (Iriref (Iri.to_string @@ Rdf.Namespace.rdf "integer")) ))
          integer
    <|> lift
          (fun d ->
            String
              ( d,
                None,
                Some (Iriref (Iri.to_string @@ Rdf.Namespace.rdf "decimal")) ))
          decimal
    <|> lift
          (fun d ->
            String
              ( d,
                None,
                Some (Iriref (Iri.to_string @@ Rdf.Namespace.rdf "double")) ))
          double
    <|> lift
          (fun b ->
            String
              ( b,
                None,
                Some (Iriref (Iri.to_string @@ Rdf.Namespace.rdf "boolean")) ))
          boolean
    <|> lift (fun s -> String (s, None, None)) string_

  let rec blanknodepropertylist seq =
    (lift (fun lst -> PredObjs lst) (left_bracket *> predobjs <* right_bracket))
      seq

  (* <iriverb> "object1", "object2"; <iriverb2> "object" *)
  and predobjs seq =
    lift2
      (fun predobjs _x -> predobjs)
      (sep_by1 (many1 semicolon) predobj)
      (* The optional SEMICOLON token is thrown away anyway. *)
      (option' [ SEMICOLON ] (many1 semicolon))
      seq

  (* <iriverb> "object", "object2" *)
  and predobj seq = (lift2 (fun a b -> (a, b)) verb (sep_by1 comma object_)) seq

  (* <iri> *)
  (* _:b [] *)
  (* () ("a" <iri>) *)
  (* [preobjs]  = [ <verb1> "a", "b"; <verb2> "a", "b2" ]*)
  (* "literal" *)
  and object_ seq =
    (lift (fun literal -> Obj_literal literal) literal
    <|> lift (fun iri -> Obj_iri iri) iri
    <|> lift (fun bnode -> Obj_blank bnode) blanknode
    <|> lift (fun coll -> Obj_blank (Collection coll)) collection
    <|> lift (fun bnodeplst -> Obj_blank bnodeplst) blanknodepropertylist)
      seq

  (* () ("literal" [ <iriverb> "object", "object2"]) *)
  and collection seq = (left_par *> many object_ <* right_par) seq

  and subject seq =
    (lift (fun iri -> Sub_iri iri) iri
    <|> lift (fun bnode -> Sub_blank bnode) blanknode
    <|> lift (fun c -> Sub_blank (Collection c)) collection)
      seq

  let triples =
    lift2 (fun s pos -> (s, pos)) subject predobjs
    <|> lift2
          (fun bs pos_opt -> (Sub_blank bs, pos_opt))
          blanknodepropertylist (option' [] predobjs)

  let statement =
    lift (fun (a, b) -> Triples (a, b)) triples
    <* dot
    <|> lift (fun d -> Directive d) directive

  let statement' = lift (fun x -> Some x) statement
end

(* The function `parse_to_statements` transforms a sequence of chars,
 * which forms a well-formed turtle document, into a sequence of
 * turtle statements. *)
let parse_to_statements char_seq =
  let rec tokenize lexbuf =
    match Lexer.main lexbuf with
    | Some token -> Seq.cons token (tokenize lexbuf)
    | None -> Seq.empty
  in
  let tokens =
    char_seq |> Seq.memoize
    (* create a Sedlex buffer *)
    |> Gen.of_seq
    |> Sedlexing.Utf8.from_gen
    (* tokenize *)
    |> tokenize
    (* make sequence persistent in order to be able to backtrace when parsing *)
    |> Seq.memoize
  in

  let rec do_parse tokens =
    match Parser.(statement' <|> eof) tokens with
    | Ok (Some x, tokens_rest) -> Seq.cons x (do_parse tokens_rest)
    | Ok (None, _) -> Seq.empty
    | Error msg -> failwith ("Parser failed: " ^ msg)
  in
  (* parse into a sequence of statements *)
  do_parse tokens

(* @TODO Not sure about this default base_iri. It makes the tests pass though *)
let decode
    ?(base_iri = Rdf.Iri.of_string "")
    ?(seed = Random.State.make_self_init ())
    seq =
  let open Turtle_types in
  let ctx = { base_iri; prefixes = SMap.empty; blanks = SMap.empty; seed } in
    seq
    |> parse_to_statements
    |> Turtle_to_rdf.read ctx

let encode
    ?(prefixes=[])
    ?(base = Rdf.Iri.of_string "")
    descriptions =

  let encode_statements seq =
    Seq.fold_left
      (fun acc s -> acc ^ (Encode.encode_statement s))
      "" seq in

  descriptions
  |> Seq.map (
    fun description -> Encode.statement_of_description ~prefixes ~base description)
  |> (fun seq ->
      (Seq.return @@ Encode.statement_of_base base)
      @@@ (List.(to_seq @@ map Encode.statement_of_prefix prefixes))
      @@@ seq)
  |> encode_statements
  |> String.to_seq
