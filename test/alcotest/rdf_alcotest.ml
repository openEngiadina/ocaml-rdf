(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let iri = Alcotest.testable Rdf.Iri.pp Rdf.Iri.equal
let blank_node = Alcotest.testable Rdf.Blank_node.pp Rdf.Blank_node.equal
let literal = Alcotest.testable Rdf.Literal.pp Rdf.Literal.equal
let subject = Alcotest.testable Rdf.Triple.Subject.pp Rdf.Triple.Subject.equal

let predicate =
  Alcotest.testable Rdf.Triple.Predicate.pp Rdf.Triple.Predicate.equal

let object' = Alcotest.testable Rdf.Triple.Object.pp Rdf.Triple.Object.equal
let term = Alcotest.testable Rdf.Term.pp Rdf.Term.equal
let triple = Alcotest.testable Rdf.Triple.pp Rdf.Triple.equal

let graph =
  Alcotest.testable Rdf.Graph.pp (fun a b ->
      if Rdf.Graph.equal a b then true
      else Rdf_graph_isomorphism_z3.isomorphic a b)
