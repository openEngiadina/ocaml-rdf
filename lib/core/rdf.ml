(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Iri = Term.Iri
module Blank_node = Blank_node
module Literal = Literal
module Term = Term
module Triple = Triple
module Namespace = Namespace
module Description = Description
module Graph = Graph
module Decoder = Decoder
