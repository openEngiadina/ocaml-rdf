(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let parse_to_graph ?base_iri seq =
  seq
  |> Rdf_turtle.decode ?base_iri
  |> Seq.fold_left
    (fun g triple -> Rdf.Graph.add triple g)
    Rdf.Graph.empty

module Term = struct
  let iri s = Rdf.Term.of_iri @@ Rdf.Iri.of_string s
  let literal l = Rdf.Term.of_literal l

  let examples =
    [
      ( iri "https://example.com/",
        [
          0xd9;
          0x01;
          0x0a;
          0x74;
          0x68;
          0x74;
          0x74;
          0x70;
          0x73;
          0x3a;
          0x2f;
          0x2f;
          0x65;
          0x78;
          0x61;
          0x6d;
          0x70;
          0x6c;
          0x65;
          0x2e;
          0x63;
          0x6f;
          0x6d;
          0x2f;
        ] );
      ( iri "https://example.com#fragment",
        [
          0xd9;
          0x01;
          0x0a;
          0x78;
          0x1c;
          0x68;
          0x74;
          0x74;
          0x70;
          0x73;
          0x3a;
          0x2f;
          0x2f;
          0x65;
          0x78;
          0x61;
          0x6d;
          0x70;
          0x6c;
          0x65;
          0x2e;
          0x63;
          0x6f;
          0x6d;
          0x23;
          0x66;
          0x72;
          0x61;
          0x67;
          0x6d;
          0x65;
          0x6e;
          0x74;
        ] );
      ( iri "urn:uuid:1da600cf-c852-469a-936f-e608d3d90d9b",
        [
          0xd8;
          0x25;
          0x50;
          0x1d;
          0xa6;
          0x00;
          0xcf;
          0xc8;
          0x52;
          0x46;
          0x9a;
          0x93;
          0x6f;
          0xe6;
          0x08;
          0xd3;
          0xd9;
          0x0d;
          0x9b;
        ] );
      ( iri "urn:uuid:1da600cf-c852-469a-936f-e608d3d90d9b#a",
        [
          0xd9;
          0x01;
          0x31;
          0x82;
          0xd8;
          0x25;
          0x50;
          0x1d;
          0xa6;
          0x00;
          0xcf;
          0xc8;
          0x52;
          0x46;
          0x9a;
          0x93;
          0x6f;
          0xe6;
          0x08;
          0xd3;
          0xd9;
          0x0d;
          0x9b;
          0x61;
          0x61;
        ] );
      ( literal @@ Rdf.Literal.make_string ~language:"en" "Hello World!",
        [
          0xd8;
          0x26;
          0x82;
          0x62;
          0x65;
          0x6e;
          0x6c;
          0x48;
          0x65;
          0x6c;
          0x6c;
          0x6f;
          0x20;
          0x57;
          0x6f;
          0x72;
          0x6c;
          0x64;
          0x21;
        ] );
      ( literal @@ Rdf.Literal.make_string "asdf",
        [ 0x64; 0x61; 0x73; 0x64; 0x66 ] );
      (literal @@ Rdf.Literal.make_boolean true, [ 0xf5 ]);
      (literal @@ Rdf.Literal.make_integer 42, [ 0x18; 0x2a ]);
      (literal @@ Rdf.Literal.make_float 1.5, [ 0xfa; 0x3f; 0xc0; 0x00; 0x00 ]);
      ( literal
        @@ Rdf.Literal.make "POINT(7.9736903 47.5412464)"
             (Rdf.Iri.of_string
                "http://www.opengis.net/ont/geosparql#wktLiteral"),
        [
          0xd9;
          0x01;
          0x2f;
          0x82;
          0xd9;
          0x01;
          0x0a;
          0x78;
          0x2f;
          0x68;
          0x74;
          0x74;
          0x70;
          0x3a;
          0x2f;
          0x2f;
          0x77;
          0x77;
          0x77;
          0x2e;
          0x6f;
          0x70;
          0x65;
          0x6e;
          0x67;
          0x69;
          0x73;
          0x2e;
          0x6e;
          0x65;
          0x74;
          0x2f;
          0x6f;
          0x6e;
          0x74;
          0x2f;
          0x67;
          0x65;
          0x6f;
          0x73;
          0x70;
          0x61;
          0x72;
          0x71;
          0x6c;
          0x23;
          0x77;
          0x6b;
          0x74;
          0x4c;
          0x69;
          0x74;
          0x65;
          0x72;
          0x61;
          0x6c;
          0x78;
          0x1b;
          0x50;
          0x4f;
          0x49;
          0x4e;
          0x54;
          0x28;
          0x37;
          0x2e;
          0x39;
          0x37;
          0x33;
          0x36;
          0x39;
          0x30;
          0x33;
          0x20;
          0x34;
          0x37;
          0x2e;
          0x35;
          0x34;
          0x31;
          0x32;
          0x34;
          0x36;
          0x34;
          0x29;
        ] );
      ( Rdf.Term.of_blank_node @@ Rdf.Blank_node.of_string "bnode0",
        [ 0xd9; 0x01; 0x30; 0x66; 0x62; 0x6e; 0x6f; 0x64; 0x65; 0x30 ] );
    ]

  let binary_testable =
    Alcotest.testable Fmt.(on_string @@ octets ()) String.equal

  let to_test (term, expected_encoding) =
    Alcotest.test_case (Format.asprintf "Example: %a" Rdf.Term.pp term) `Quick
      (fun () ->
        let expected_encoding =
          expected_encoding |> List.to_seq |> Seq.map Char.chr |> String.of_seq
        in

        let cbor = Rdf_cbor.Term.encode term in

        Format.printf "CBOR: %a\n" Cborl.pp cbor;

        let encoded = cbor |> Cborl.write |> String.of_seq in

        Alcotest.check binary_testable "encoded term matches expected encoding"
          expected_encoding encoded;

        let decoded =
          encoded |> String.to_seq |> Cborl.read |> Rdf_cbor.Term.decode
        in

        Alcotest.check Rdf_alcotest.term
          "decoded term is equal to original term" term decoded)

  let encode_decode_test =
    QCheck_alcotest.to_alcotest
    @@ QCheck.Test.make ~count:1000 ~name:"Random terms" Rdf_gen.term
         (fun term ->
           let cbor = Rdf_cbor.Term.encode term in
           let decoded =
             cbor |> Cborl.write |> Cborl.read |> Rdf_cbor.Term.decode
           in
           (* Alcotest.check item_testable "decodes is equal to encoded item" item *)
           (* decoded; *)
           Alcotest.check Rdf_alcotest.term
             "encoded and decoded term is equal to term" decoded term;
           true)

  let test_cases = encode_decode_test :: List.map to_test examples
end

let seq_of_file file =
  let ic = open_in file in
  Seq.of_dispenser (fun () -> In_channel.input_char ic)

module Molecule = struct
  let example =
    Alcotest.test_case "Example" `Quick (fun () ->
        let graph = parse_to_graph (seq_of_file "example.ttl") in

        let cbor = graph |> Rdf_cbor.Molecule.encode in

        let decoded =
          cbor |> Cborl.write |> Cborl.read |> Rdf_cbor.Molecule.decode
        in

        Alcotest.check Rdf_alcotest.graph
          "encoded and decoded molecule is same graph." graph decoded;

        ())

  let random =
    QCheck_alcotest.to_alcotest
    @@ QCheck.Test.make ~count:100 ~name:"Random graphs" Rdf_gen.graph
         (fun graph ->
           let decoded =
             graph |> Rdf_cbor.Molecule.encode |> Cborl.write |> Cborl.read
             |> Rdf_cbor.Molecule.decode
           in

           Alcotest.check Rdf_alcotest.graph
             "encoded and decoded term is equal to term" graph decoded;
           true)

  let test_cases = [ random; example ]
end

let blake2b_hash s =
  let ctx = Digestif.BLAKE2B.digest_string s in
  let hash = Digestif.BLAKE2B.to_raw_string ctx in
  let hash_b32 = Base32.encode_string ~pad:false hash in
  Rdf.Iri.of_string @@ "urn:blake2b:" ^ hash_b32

module Content_addressable = struct
  let ca_example =
    Alcotest.test_case "Example" `Quick (fun () ->
        let graph = parse_to_graph (seq_of_file "ca-example.ttl") in

        let fragment =
          graph |> Rdf.Graph.to_triples
          |> Rdf_cbor.Content_addressable.of_triples |> Seq.uncons |> Option.get
          |> fst |> snd
        in

        let urn =
          Rdf_cbor.Content_addressable.base_subject ~hash:blake2b_hash fragment
        in

        Alcotest.check Rdf_alcotest.iri
          "Computed base subject matches expectation"
          (Rdf.Iri.of_string
             "urn:blake2b:7B6VYVGTSQC7KWXANVA4PYUP6VDGSNIOLYX4QLY7AF5CKHAIMJ4QE7U3DTGPCSSFEW4PIJ4OFZ4AEZVYEOZV3KW476RDGUFZR4JGOOY")
          urn)

  let test_cases = [ ca_example ]
end

module Stream = struct
  let stream_test =
    QCheck_alcotest.to_alcotest
    @@ QCheck.Test.make ~count:10 ~name:"Random stream of graphs"
         (QCheck.list Rdf_gen.graph) (fun graphs ->
           let decoded =
             graphs |> List.to_seq
             |> Seq.map (fun graph -> Rdf_cbor.Stream.Molecule graph)
             |> Rdf_cbor.Stream.write |> Cborl.Signal.write |> Cborl.Signal.read
             |> Rdf_cbor.Stream.read ~hash:blake2b_hash
             |> Rdf.Graph.of_triples
           in

           let graph_in =
             graphs |> List.to_seq
             |> Seq.fold_left Rdf.Graph.union Rdf.Graph.empty
           in

           (* Alcotest.check item_testable "decodes is equal to encoded item" item *)
           (* decoded; *)
           Alcotest.check Rdf_alcotest.graph
             "encoded and decoded term is equal to term" graph_in decoded;
           true)

  let test_cases = [ stream_test ]
end

let () =
  Alcotest.run "RDF/CBOR"
    [
      ("Term encoding", Term.test_cases);
      ("Molecule", Molecule.test_cases);
      ("Content-addressable", Content_addressable.test_cases);
      ("Stream", Stream.test_cases);
    ]
