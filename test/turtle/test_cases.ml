(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 * SPDX-FileCopyrightText: 2022 alleycat <arie@alleycat.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let seq_of_file file =
  let ic = open_in file in
  Seq.of_dispenser (fun () -> In_channel.input_char ic)

let parse_to_graph ?base_iri seq =
  seq
  |> Rdf_turtle.decode ?base_iri
  |> Seq.fold_left
    (fun g triple -> Rdf.Graph.add triple g)
    Rdf.Graph.empty

let mf =
  Rdf.Namespace.make_namespace
    "http://www.w3.org/2001/sw/DataAccess/tests/test-manifest#"

let rdft = Rdf.Namespace.make_namespace "http://www.w3.org/ns/rdftest#"

let seq_next seq =
  match seq () with Seq.Cons (value, _) -> Some value | Seq.Nil -> None

let get_literal graph subject predicate =
  Option.bind
    (seq_next @@ Rdf.Graph.objects subject predicate graph)
    (fun o -> Rdf.Term.to_literal @@ Rdf.Triple.Object.to_term o)

let get_iri graph subject predicate =
  Option.bind
    (seq_next @@ Rdf.Graph.objects subject predicate graph)
    (fun o -> Rdf.Term.to_iri @@ Rdf.Triple.Object.to_term o)

let read_ntriples iri =
  let file = "TurtleTests/" ^ Rdf.Iri.to_string iri in
  seq_of_file file |> Rdf_ntriples.parse |> Rdf.Graph.of_triples

let read_rdf_turtle iri =
  let file = "TurtleTests/" ^ Rdf.Iri.to_string iri in
  let base_iri =
    "http://www.w3.org/2013/TurtleTests/" ^ Rdf.Iri.to_string iri
    |> Rdf.Iri.of_string
  in
  let turtle_char_seq = seq_of_file file in
  (* Rdf_turtle.decode turtle_string *)
  parse_to_graph ~base_iri turtle_char_seq

let skip_turtle_eval_test_cases = function
  (*
    @TODO the test <#turtle-syntax-bad-LITERAL_2_with_langtag_and_datatype>
    has name "turtle-syntax-bad-num-05" in the manifest.ttl file.
    This shouldn't be the case, and we could inform w3c, I guess.
  *)
  (* The following tests don't have a file with n-triples, while they should have one *)
  | "turtle-eval-bad-01" -> Some ".nt file is missing"
  | "turtle-eval-bad-02" -> Some ".nt file is missing"
  | "turtle-eval-bad-03" -> Some ".nt file is missing"
  | "turtle-eval-bad-04" -> Some ".nt file is missing"
  | _ -> None

let skip_turtle_syntax_test_cases = function _ -> None

let test_case_speed = function
  | "nested_collection" | "first" | "last" | "turtle-subm-06" | "turtle-subm-08"
    ->
      (* checking isomorphism takes long
         disable slow tests with test_case.exe -q *)
      `Slow
  | _ -> `Quick

let name graph case =
  get_literal graph case (Rdf.Triple.Predicate.of_iri @@ mf "name")
  |> Option.map Rdf.Literal.canonical
  |> Option.get

let action graph case =
  get_iri graph case (Rdf.Triple.Predicate.of_iri @@ mf "action") |> Option.get

let expected graph case =
  get_iri graph case (Rdf.Triple.Predicate.of_iri @@ mf "result")
  |> Option.get |> read_ntriples

let test_turtle_eval graph case =
  let name = name graph case in
  match skip_turtle_eval_test_cases name with
  | Some reason ->
      ( Format.sprintf "%s - SKIPPED (%s)" name reason,
        [ Alcotest.test_case "SKIPPED" `Quick (fun () -> ()) ] )
  | None ->
      let action = action graph case in
      let expected = expected graph case in
      ( name,
        [
          Alcotest.test_case "TestTurtleEval" (test_case_speed name) (fun () ->
              let parsed_graph = read_rdf_turtle action in
              Alcotest.check Rdf_alcotest.graph
                "parsed Turtle is equal to result" expected parsed_graph);
        ] )

let test_turtle_negative_eval graph case =
  let name = name graph case in
  match skip_turtle_eval_test_cases name with
  | Some reason ->
      ( Format.sprintf "%s - SKIPPED (%s)" name reason,
        [ Alcotest.test_case "SKIPPED" `Quick (fun () -> ()) ] )
  | None ->
      let action = action graph case in
      let expected = expected graph case in
      let parsed_graph = read_rdf_turtle action in
      ( name,
        [
          Alcotest.test_case "TestTurtleNegativeEval" (test_case_speed name)
            (fun () ->
              (* @TODO does this work (it seems so, using utop)?
                 however, we don't know about it yet, since there are no NegativeEval tests
                 (they all miss the .nt file) *)
              Alcotest.check
                (Alcotest.neg Rdf_alcotest.graph)
                "parsed Turtle is not equal to result" expected parsed_graph);
        ] )

let test_turtle_negative_syntax graph case =
  let name = name graph case in
  match skip_turtle_syntax_test_cases name with
  | Some reason ->
      ( Format.sprintf "%s - SKIPPED (%s)" name reason,
        [ Alcotest.test_case "SKIPPED" `Quick (fun () -> ()) ] )
  | None ->
      let action = action graph case in
      ( name,
        [
          Alcotest.test_case "TestTurtleNegativeSyntax" `Quick (fun () ->
              try
                ignore @@ read_rdf_turtle action;
                raise @@ Failure "Parsing succeeded when expecting error"
              with
              | Invalid_argument _ -> ()
              (* @TODO here, we catch the error that is raised from the Serd parser. I don't think it should work like this. *)
              | _e -> ());
        ] )

let test_turtle_positive_syntax graph case =
  let name = name graph case in
  let action = action graph case in
  match skip_turtle_syntax_test_cases name with
  | Some reason ->
      ( Format.sprintf "%s - SKIPPED (%s)" name reason,
        [ Alcotest.test_case "SKIPPED" `Quick (fun () -> ()) ] )
  | None ->
      ( name,
        [
          Alcotest.test_case "TestTurtlePositiveSyntax" `Quick (fun () ->
              (* @TODO check this try with block *)
              try ignore @@ read_rdf_turtle action with
              | Invalid_argument s ->
                  raise
                  @@ Failure
                       (Format.sprintf "Parsing not succeeded, reason: %s" s)
              | e -> raise e);
        ] )

let to_alcotest graph case =
  let type' =
    Option.bind
      (Rdf.Graph.objects case
         (Rdf.Triple.Predicate.of_iri @@ Rdf.Namespace.rdf "type")
         graph
      |> seq_next)
      (fun o -> Rdf.Term.to_iri @@ Rdf.Triple.Object.to_term o)
  in
  match type' with
  | Some type' when type' = rdft "TestTurtleEval" ->
      test_turtle_eval graph case
  | Some type' when type' = rdft "TestTurtleNegativeEval" ->
      test_turtle_negative_eval graph case
  | Some type' when type' = rdft "TestTurtlePositiveSyntax" ->
      test_turtle_positive_syntax graph case
  | Some type' when type' = rdft "TestTurtleNegativeSyntax" ->
      test_turtle_negative_syntax graph case
  | Some type' ->
      raise
      @@ Invalid_argument
           (Format.asprintf "Unknown test case type: %a" Rdf.Iri.pp type')
  | None -> raise @@ Invalid_argument "Test case does not have type"

let read_manifest file =
  let manifest_graph = seq_of_file file |> parse_to_graph in

  let x =
    Rdf.Graph.collection
      (Rdf.Triple.Subject.of_iri @@ Rdf.Iri.of_string "")
      (Rdf.Triple.Predicate.of_iri @@ mf "entries")
      manifest_graph
  in
  x
  |> Seq.filter_map Rdf.Term.to_iri
  |> Seq.map Rdf.Triple.Subject.of_iri
  |> Seq.map (to_alcotest manifest_graph)
  |> List.of_seq

let () =
  let test_cases = read_manifest "TurtleTests/manifest.ttl" in
  Alcotest.run "rdf_turtle" test_cases
