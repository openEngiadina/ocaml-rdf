(*
 * SPDX-FileCopyrightText: 2022 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let seq_of_file file =
  let ic = open_in file in
  Seq.of_dispenser (fun () -> In_channel.input_char ic)

let parse_to_graph ?base_iri seq =
  seq
  |> Rdf_turtle.decode ?base_iri
  |> Seq.fold_left
    (fun g triple -> Rdf.Graph.add triple g)
    Rdf.Graph.empty

let mf =
  Rdf.Namespace.make_namespace
    "http://www.w3.org/2001/sw/DataAccess/tests/test-manifest#"

let rdft = Rdf.Namespace.make_namespace "http://www.w3.org/ns/rdftest#"

let read_ntriples iri =
  let file = "N-TriplesTests/" ^ Rdf.Iri.to_string iri in
  seq_of_file file |> Rdf_ntriples.parse
  |> Seq.fold_left (fun g triple -> Rdf.Graph.add triple g) Rdf.Graph.empty

let test_positive_syntax description =
  let name =
    Rdf.Description.functional_property_literal
      (Rdf.Triple.Predicate.of_iri @@ mf "name")
      description
    |> Option.get |> Rdf.Literal.canonical
  in

  let action =
    Rdf.Description.functional_property_iri
      (Rdf.Triple.Predicate.of_iri @@ mf "action")
      description
    |> Option.get
  in

  ( name,
    [
      Alcotest.test_case "TestNTriplesPositiveSyntax" `Quick (fun () ->
          try ignore @@ read_ntriples action with
          | Invalid_argument s ->
              raise
              @@ Failure (Format.sprintf "Parsing not succeeded, reason: %s" s)
          | e -> raise e);
    ] )

let test_negative_syntax description =
  let name =
    Rdf.Description.functional_property_literal
      (Rdf.Triple.Predicate.of_iri @@ mf "name")
      description
    |> Option.get |> Rdf.Literal.canonical
  in

  let action =
    Rdf.Description.functional_property_iri
      (Rdf.Triple.Predicate.of_iri @@ mf "action")
      description
    |> Option.get
  in
  ( name,
    [
      Alcotest.test_case "TestNTriplesNegativeSyntax" `Quick (fun () ->
          try
            ignore @@ read_ntriples action;
            raise @@ Failure "Parsing succeeded when expecting error"
          with
          | Invalid_argument _ -> ()
          | e -> raise e);
    ] )

let to_alcotest description =
  let type' =
    Rdf.Description.functional_property_iri
      (Rdf.Triple.Predicate.of_iri @@ Rdf.Namespace.rdf "type")
      description
  in
  match type' with
  | Some type' when type' = rdft "TestNTriplesPositiveSyntax" ->
      test_positive_syntax description
  | Some type' when type' = rdft "TestNTriplesNegativeSyntax" ->
      test_negative_syntax description
  | Some type' ->
      raise
      @@ Invalid_argument
           (Format.asprintf "Unknown test case type: %a" Rdf.Iri.pp type')
  | None -> raise @@ Invalid_argument "Test case does not have type"

let read_manifest file =
  let manifest_graph = seq_of_file file |> parse_to_graph in

  (* Read manifest entries to Seq of IRIs *)
  Rdf.Graph.collection
    (Rdf.Triple.Subject.of_iri @@ Rdf.Iri.of_string "")
    (Rdf.Triple.Predicate.of_iri @@ mf "entries")
    manifest_graph
  |> Seq.filter_map Rdf.Term.to_iri
  |> Seq.map (fun iri ->
         Rdf.Graph.description (Rdf.Triple.Subject.of_iri iri) manifest_graph)
  |> Seq.map to_alcotest |> List.of_seq

let () =
  let test_cases = read_manifest "N-TriplesTests/manifest.ttl" in
  Alcotest.run "rdf_ntriples" test_cases
