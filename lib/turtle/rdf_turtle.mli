(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 * SPDX-FileCopyrightText: 2022 alleycat <arie@alleycat.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** Implementation of the RDF/Turtle serialization.

    @see <https://www.w3.org/TR/turtle/> *)

(** {1 Parser} *)

val decode :
  ?base_iri:Rdf.Iri.t ->
  ?seed:Random.State.t ->
  char Seq.t ->
  Rdf.Triple.t Seq.t
(** [decode ~base_iri ~seed seq] reads a sequence of characters and transforms it, if it is a well-formed turtle document, into the corresponding sequence of Rdf.Triples. It raises an error otheriwse. See https://www.w3.org/TR/turtle/#sec-parsing. *)

(** {1 Encoding} *)

val encode :
  ?prefixes:(string * Rdf.Iri.t) list ->
  ?base:Rdf.Iri.t ->
  Rdf.Description.t Seq.t ->
  char Seq.t
(** [encode ~prefixes ~base descriptions] returns a sequence of characters holding the serializaiton of the sequence of descriptions [descriptions]. *)
