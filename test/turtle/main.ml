let seq_of_file file =
  let ic = open_in file in
  Seq.of_dispenser (fun () -> In_channel.input_char ic)

let parse_to_graph ?base_iri seq =
  seq
  |> Rdf_turtle.decode ?base_iri
  |> Seq.fold_left
    (fun g triple -> Rdf.Graph.add triple g)
    Rdf.Graph.empty

let mf =
  Rdf.Namespace.make_namespace
    "http://www.w3.org/2001/sw/DataAccess/tests/test-manifest#"

let rdft = Rdf.Namespace.make_namespace "http://www.w3.org/ns/rdftest#"

let seq_next seq =
  match seq () with Seq.Cons (value, _) -> Some value | Seq.Nil -> None

let get_literal graph subject predicate =
  Option.bind
    (seq_next @@ Rdf.Graph.objects subject predicate graph)
    (fun o -> Rdf.Term.to_literal @@ Rdf.Triple.Object.to_term o)

let get_iri graph subject predicate =
  Option.bind
    (seq_next @@ Rdf.Graph.objects subject predicate graph)
    (fun o -> Rdf.Term.to_iri @@ Rdf.Triple.Object.to_term o)

let read_rdf_turtle iri =
  "TurtleTests/" ^ Rdf.Iri.to_string iri
  |> seq_of_file
  |> Rdf_turtle.decode
  |> Rdf.Graph.of_triples

let encoded_decoded g =
  g |> Rdf.Graph.descriptions |> Rdf_turtle.encode
  |> Rdf_turtle.decode |> Rdf.Graph.of_triples

let name graph case =
  get_literal graph case (Rdf.Triple.Predicate.of_iri @@ mf "name")
  |> Option.map Rdf.Literal.canonical
  |> Option.get

let action graph case =
  get_iri graph case (Rdf.Triple.Predicate.of_iri @@ mf "action") |> Option.get

let skip_turtle_encode_decode_test_cases = function
  | "turtle-eval-bad-01" -> Some ".nt file is missing"
  | "turtle-eval-bad-02" -> Some ".nt file is missing"
  | "turtle-eval-bad-03" -> Some ".nt file is missing"
  | "turtle-eval-bad-04" -> Some ".nt file is missing"
  | "nested_collection" | "first" | "last"
  | "turtle-subm-06" | "turtle-subm-08"
  (* this one takes a very, very long time *)
  | "turtle-syntax-lists-05" -> Some "Too slow"
  | _ -> None

let test_turtle_encode_decode graph case =
  let name = name graph case in
  let action = action graph case in
  let case_graph = read_rdf_turtle action in
  let encoded_decoded = encoded_decoded case_graph in
  match skip_turtle_encode_decode_test_cases name with
  | Some reason ->
      ( Format.sprintf "%s - SKIPPED (%s)" name reason,
        [ Alcotest.test_case "SKIPPED" `Quick (fun () -> ()) ] )
  | None ->
  ( name,
    [
      Alcotest.test_case "TestTurtleEval" `Quick (fun () ->
          Alcotest.check Rdf_alcotest.graph
            "parsed Turtle is equal to result" case_graph encoded_decoded);
    ] )

let skip_test_case graph case =
  let name = name graph case in
      ( Format.sprintf "%s - SKIPPED: not useful for encode_decode " name,
        [ Alcotest.test_case "SKIPPED" `Quick (fun () -> ()) ] )

let to_alcotest graph case =
  let type' =
    Option.bind
      (Rdf.Graph.objects case
         (Rdf.Triple.Predicate.of_iri @@ Rdf.Namespace.rdf "type")
         graph
       |> seq_next)
      (fun o -> Rdf.Term.to_iri @@ Rdf.Triple.Object.to_term o)
  in
  match type' with
  | Some type' when type' = rdft "TestTurtleEval" ->
    test_turtle_encode_decode graph case
  | Some type' when type' = rdft "TestTurtleNegativeEval" ->
    skip_test_case graph case
  | Some type' when type' = rdft "TestTurtlePositiveSyntax" ->
    test_turtle_encode_decode graph case
  | Some type' when type' = rdft "TestTurtleNegativeSyntax" ->
    skip_test_case graph case
  | Some type' ->
    raise
    @@ Invalid_argument
      (Format.asprintf "Unknown test case type: %a" Rdf.Iri.pp type')
  | None -> raise @@ Invalid_argument "Test case does not have type"

let read_manifest file =
  let manifest_graph = seq_of_file file |> parse_to_graph in
  let x =
    Rdf.Graph.collection
      (Rdf.Triple.Subject.of_iri @@ Rdf.Iri.of_string "")
      (Rdf.Triple.Predicate.of_iri @@ mf "entries")
      manifest_graph
  in
  x
  |> Seq.filter_map Rdf.Term.to_iri
  |> Seq.map Rdf.Triple.Subject.of_iri
  |> Seq.map (to_alcotest manifest_graph)
  |> List.of_seq

let () =
  let test_cases = read_manifest "TurtleTests/manifest.ttl" in
  Alcotest.run "rdf_turtle: encode_decode" test_cases
