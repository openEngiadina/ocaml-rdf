(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(* Encoding *)

(* In the following we define some helpers for mapping an Rdf.Graph to Z3.

   RDF terms are encoded as integers using `Hashtbl.hash`. Triples are
   encoded as Triples (3-ary tuples) and a Graph is encoded as a Set
   (sort (Array Triple Bool)).

   We also encode a Set of blank nodes (this is required when defining
   constraints on the mapping between graphs).
*)

module Triple = struct
  (* Encode a Rdf.Triple as a Z3 Triple *)

  let mk_sort ctx =
    Z3.Tuple.mk_sort ctx
      (Z3.Symbol.mk_string ctx "Triple")
      [
        Z3.Symbol.mk_string ctx "subject";
        Z3.Symbol.mk_string ctx "predicate";
        Z3.Symbol.mk_string ctx "object";
      ]
      [
        Z3.Arithmetic.Integer.mk_sort ctx;
        Z3.Arithmetic.Integer.mk_sort ctx;
        Z3.Arithmetic.Integer.mk_sort ctx;
      ]

  let mk_triple sort s p o =
    let mk_triple_f = Z3.Tuple.get_mk_decl sort in
    Z3.FuncDecl.apply mk_triple_f [ s; p; o ]

  let encode ctx sort s p o =
    mk_triple sort
      (Z3.Arithmetic.Integer.mk_numeral_i ctx @@ Hashtbl.hash s)
      (Z3.Arithmetic.Integer.mk_numeral_i ctx @@ Hashtbl.hash p)
      (Z3.Arithmetic.Integer.mk_numeral_i ctx @@ Hashtbl.hash o)

  let subject sort triple =
    let get_subject = Z3.Tuple.get_field_decls sort |> List.hd in
    Z3.FuncDecl.apply get_subject [ triple ]

  let predicate sort triple =
    let get_predicate =
      Z3.Tuple.get_field_decls sort |> fun l -> List.nth l 1
    in
    Z3.FuncDecl.apply get_predicate [ triple ]

  let object' sort triple =
    let get_object' = Z3.Tuple.get_field_decls sort |> fun l -> List.nth l 2 in
    Z3.FuncDecl.apply get_object' [ triple ]
end

module Graph = struct
  (* Encode a Rdf.Graph as a Set of Triples *)

  let encode ctx triple_sort graph =
    let int_sort = Z3.Arithmetic.Integer.mk_sort ctx in
    Seq.fold_left
      (fun (graph_expr, bnode_set_expr) (triple : Rdf.Triple.t) ->
        let subject = Rdf.Triple.Subject.to_term triple.subject in
        let predicate = Rdf.Triple.Predicate.to_term triple.predicate in
        let object' = Rdf.Triple.Object.to_term triple.object' in
        let triple_expr =
          Triple.encode ctx triple_sort subject predicate object'
        in
        let bnode_set_expr =
          if Option.is_some @@ Rdf.Term.to_blank_node subject then
            Z3.Set.mk_set_add ctx bnode_set_expr
            @@ Z3.Arithmetic.Integer.mk_numeral_i ctx
            @@ Hashtbl.hash subject
          else bnode_set_expr
        in
        let bnode_set_expr =
          if Option.is_some @@ Rdf.Term.to_blank_node object' then
            Z3.Set.mk_set_add ctx bnode_set_expr
            @@ Z3.Arithmetic.Integer.mk_numeral_i ctx
            @@ Hashtbl.hash object'
          else bnode_set_expr
        in
        (Z3.Set.mk_set_add ctx graph_expr triple_expr, bnode_set_expr))
      Z3.Set.(mk_empty ctx triple_sort, mk_empty ctx int_sort)
      (Rdf.Graph.to_triples graph)
end

let isomorphic graph_a graph_b =
  (* iinitialize Z3 context and create a solver *)
  let ctx = Z3.mk_context [] in
  let solver = Z3.Solver.mk_simple_solver ctx in

  (* declare some Z3 sorts *)
  let int_sort = Z3.Arithmetic.Integer.mk_sort ctx in
  let triple_sort = Triple.mk_sort ctx in

  (* encode Graph A and B *)
  let graph_a, bnodes_graph_a = Graph.encode ctx triple_sort graph_a in
  let graph_b, bnodes_graph_b = Graph.encode ctx triple_sort graph_b in

  (* declare the mapping function from graph a to b. Note this goes
     from nodes to nodes and not from triple to triple. *)
  let f_decl = Z3.FuncDecl.mk_func_decl_s ctx "f" [ int_sort ] int_sort in
  (* The inverse function *)
  let g_decl = Z3.FuncDecl.mk_func_decl_s ctx "g" [ int_sort ] int_sort in

  (* some helpers to make the formula more readable *)
  let in' el set = Z3.Set.mk_membership ctx el set in
  let mk_triple = Triple.mk_triple triple_sort in
  let subject = Triple.subject triple_sort in
  let predicate = Triple.predicate triple_sort in
  let object' = Triple.object' triple_sort in

  (* bound symbols in quantifiers *)
  let x_sym = Z3.Symbol.mk_string ctx "x" in
  let x = Z3.Quantifier.mk_bound ctx 0 triple_sort in

  (* Function applications *)
  let f x = Z3.FuncDecl.apply f_decl [ x ] in
  let g x = Z3.FuncDecl.apply g_decl [ x ] in

  (* Quantifier that ensures that:
     - f maps from Graph A to B
     - Every Blank Node is mapped to a Blank Node
     - Every term that is not a Blank Node is mapped to itself *)
  let q_f =
    let open Z3.Boolean in
    (* quantify over all triples *)
    Z3.Quantifier.mk_forall ctx [ triple_sort ] [ x_sym ]
      (* If triple x is in Graph A *)
      (mk_implies ctx (in' x graph_a)
      (* then *)
      @@ mk_and ctx
           [
             (* the mapped triples are in Graph B *)
             in'
               (mk_triple (f @@ subject x) (predicate x) (f @@ object' x))
               graph_b;
             (* if subject is a Blank Node *)
             mk_ite ctx
               (in' (subject x) bnodes_graph_a)
               (* Then mapped subject is also a blank node *)
               (in' (f @@ subject x) bnodes_graph_b)
               (* else subject maps to itself *)
               (mk_eq ctx (subject x) (f @@ subject x));
             (* if object is a Blank Node *)
             mk_ite ctx
               (in' (object' x) bnodes_graph_a)
               (* Then mapped subject is also a blank node *)
               (in' (f @@ object' x) bnodes_graph_b)
               (* else object maps to itself *)
               (mk_eq ctx (object' x) (f @@ object' x));
           ])
      None [] [] None None
    |> Z3.Quantifier.expr_of_quantifier
  in

  (* Quantifier that ensures that f is a bijection (g is the reverse
     function). *)
  let q_g =
    let open Z3.Boolean in
    Z3.Quantifier.mk_forall ctx [ triple_sort ] [ x_sym ]
      (* if x is in Graph A *)
      (mk_implies ctx (in' x graph_a)
         (* then *)
         (* x is equal to (g (f x)) (with predicate not mapped) *)
         (mk_eq ctx x
            (mk_triple (g (f (subject x))) (predicate x) (g (f (object' x))))))
      None [] [] None None
    |> Z3.Quantifier.expr_of_quantifier
  in

  match Z3.Solver.check solver [ q_f; q_g ] with
  | Z3.Solver.SATISFIABLE -> true
  | _ -> false
