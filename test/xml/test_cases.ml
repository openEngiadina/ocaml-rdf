(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let seq_of_file file =
  let ic = open_in file in
  Seq.of_dispenser (fun () -> In_channel.input_char ic)

let parse_to_graph ?base_iri seq =
  seq
  |> Rdf_turtle.decode ?base_iri
  |> Seq.fold_left
    (fun g triple -> Rdf.Graph.add triple g)
    Rdf.Graph.empty

let mf =
  Rdf.Namespace.make_namespace
    "http://www.w3.org/2001/sw/DataAccess/tests/test-manifest#"

let rdft = Rdf.Namespace.make_namespace "http://www.w3.org/ns/rdftest#"

let read_ntriples iri =
  let file = "RDFXMLTests/" ^ Rdf.Iri.to_string iri in
  seq_of_file file |> Rdf_ntriples.parse |> Rdf.Graph.of_triples

let read_rdf_xml iri =
  let file = "RDFXMLTests/" ^ Rdf.Iri.to_string iri in
  let base_iri =
    "http://www.w3.org/2013/RDFXMLTests/" ^ Rdf.Iri.to_string iri
    |> Rdf.Iri.of_string
  in
  let xml_string = seq_of_file file |> String.of_seq in
  Xmlm.make_input ~strip:true (`String (0, xml_string))
  |> Rdf_xml.xmlm_input_to_seq
  |> Rdf_xml.parse_to_graph ~base_iri

let skip_xml_eval_test_cases = function
  | "xml-canon-test001" -> Some "XML canonicalisation not supported"
  | _ -> None

let test_case_speed = function
  | "rdf-containers-syntax-vs-schema-test002"
  | "rdf-containers-syntax-vs-schema-test004"
  | "rdfms-not-id-and-resource-attr-test001"
  | "rdfms-seq-representation-test001" | "rdfms-syntax-incomplete-test004" ->
      (* many blank nodes, checking isomorphism takes long *)
      `Slow
  | _ -> `Quick

let test_xml_eval_test_case description =
  let name =
    Rdf.Description.functional_property_literal
      (Rdf.Triple.Predicate.of_iri @@ mf "name")
      description
    |> Option.get |> Rdf.Literal.canonical
  in

  let action =
    Rdf.Description.functional_property_iri
      (Rdf.Triple.Predicate.of_iri @@ mf "action")
      description
    |> Option.get
  in

  let expected =
    Rdf.Description.functional_property_iri
      (Rdf.Triple.Predicate.of_iri @@ mf "result")
      description
    |> Option.get |> read_ntriples
  in

  match skip_xml_eval_test_cases name with
  | Some reason ->
      ( Format.sprintf "%s - SKIPPED (%s)" name reason,
        [ Alcotest.test_case "SKIPPED" `Quick (fun () -> ()) ] )
  | None ->
      ( name,
        [
          Alcotest.test_case "TestXMLEval" (test_case_speed name) (fun () ->
              let parsed_graph = read_rdf_xml action in
              Alcotest.check Rdf_alcotest.graph "parsed XML is equal to result"
                expected parsed_graph);
        ] )

let test_xml_negative_syntax description =
  let name =
    Rdf.Description.functional_property_literal
      (Rdf.Triple.Predicate.of_iri @@ mf "name")
      description
    |> Option.get |> Rdf.Literal.canonical
  in

  let action =
    Rdf.Description.functional_property_iri
      (Rdf.Triple.Predicate.of_iri @@ mf "action")
      description
    |> Option.get
  in
  ( name,
    [
      Alcotest.test_case "TestXMLNegativeSyntax" `Quick (fun () ->
          try
            ignore @@ read_rdf_xml action;
            raise @@ Failure "Parsing succeeded when expecting error"
          with
          | Invalid_argument _ -> ()
          | e -> raise e);
    ] )

let to_alcotest description =
  let type' =
    Rdf.Description.functional_property_iri
      (Rdf.Triple.Predicate.of_iri @@ Rdf.Namespace.rdf "type")
      description
  in
  match type' with
  | Some type' when type' = rdft "TestXMLEval" ->
      test_xml_eval_test_case description
  | Some type' when type' = rdft "TestXMLNegativeSyntax" ->
      test_xml_negative_syntax description
  | Some type' ->
      raise
      @@ Invalid_argument
           (Format.asprintf "Unknown test case type: %a" Rdf.Iri.pp type')
  | None -> raise @@ Invalid_argument "Test case does not have type"

let read_manifest file =
  let manifest_graph = seq_of_file file |> parse_to_graph in

  (* Read manifest entries to Seq of IRIs *)
  Rdf.Graph.collection
    (Rdf.Triple.Subject.of_iri @@ Rdf.Iri.of_string "")
    (Rdf.Triple.Predicate.of_iri @@ mf "entries")
    manifest_graph
  |> Seq.filter_map Rdf.Term.to_iri
  |> Seq.map (fun iri ->
         Rdf.Graph.description (Rdf.Triple.Subject.of_iri iri) manifest_graph)
  |> Seq.map to_alcotest |> List.of_seq

let () =
  let test_cases = read_manifest "RDFXMLTests/manifest.ttl" in
  Alcotest.run "rdf_xml" test_cases
