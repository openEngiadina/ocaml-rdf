(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let ex = Rdf.Namespace.make_namespace "http://example.com/"
let ex2 = Rdf.Namespace.make_namespace "http://example2.com/"

module TermTest = struct
  let compare_unit_test =
    let open Alcotest in
    test_case "Rdf.Term.compare" `Quick (fun () ->
        check int "lu" 1
          Rdf.Term.(
            compare
              (of_literal @@ Rdf.Literal.make "hi" (Rdf.Namespace.xsd "string"))
              (of_blank_node @@ Rdf.Blank_node.of_string "hi")))

  let compare_bnode_literal_property_test =
    QCheck.Test.make ~name:"order between blank nodes and literals"
      (QCheck.pair Rdf_gen.blank_node Rdf_gen.literal) (fun (bnode, literal) ->
        Alcotest.check Alcotest.int "compare bnode to literal is always -1" (-1)
          Rdf.Term.(compare (of_blank_node bnode) (of_literal literal));
        Alcotest.check Alcotest.int "compare bnode to literal is always 1" 1
          Rdf.Term.(compare (of_literal literal) (of_blank_node bnode));
        true)

  let test_cases =
    [
      compare_unit_test;
      QCheck_alcotest.to_alcotest compare_bnode_literal_property_test;
    ]
end

module GraphTest = struct
  let equal_unit_test =
    let open Alcotest in
    test_case "equality unit tests" `Quick (fun () ->
        check bool "empty graphs are equal" true Rdf.Graph.(equal empty empty);

        check bool "graph with one element added is not equal to empty graph"
          false
          Rdf.Graph.(
            equal empty
              (empty
              |> add
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_iri @@ ex "bar"))));

        check bool "graph with one element removed is equal to empty graph" true
          Rdf.Graph.(
            equal empty
              (empty
              |> add
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_iri @@ ex "bar"))
              |> remove
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_iri @@ ex "bar"))));

        check bool "graph with one element is equal" true
          Rdf.Graph.(
            equal
              (empty
              |> add
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_iri @@ ex "bar")))
              (empty
              |> add
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_iri @@ ex "bar"))));

        check bool "graph with same literal is equal" true
          Rdf.Graph.(
            equal
              (empty
              |> add
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_literal
                       @@ Rdf.Literal.make "hello" (Rdf.Namespace.xsd "string")
                       )))
              (empty
              |> add
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_literal
                       @@ (* Try and force physical inequality *)
                       Rdf.Literal.make ("h" ^ "ello")
                         (Rdf.Namespace.xsd "string")))));

        check bool "graph with two different elements is not equal" false
          Rdf.Graph.(
            equal
              (empty
              |> add
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_iri @@ ex "bar")))
              (empty
              |> add
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex2 "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_iri @@ ex "bar"))));

        check bool
          "graph with blank node and literal added in different order is equal"
          true
          Rdf.Graph.(
            equal
              (empty
              |> add
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_blank_node @@ Rdf.Blank_node.of_string "b0"))
              |> add
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_literal
                       @@ Rdf.Literal.make "hi" (Rdf.Namespace.xsd "string"))))
              (empty
              |> add
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_literal
                       @@ Rdf.Literal.make "hi" (Rdf.Namespace.xsd "string")))
              |> add
                   Rdf.Triple.(
                     make
                       (Subject.of_iri @@ ex "foo")
                       (Predicate.of_iri @@ ex "prop")
                       (Object.of_blank_node @@ Rdf.Blank_node.of_string "b0"))
              )))

  let shuffle lst =
    let shuffle arr =
      for n = Array.length arr - 1 downto 1 do
        let k = Random.int (n + 1) in
        let temp = arr.(n) in
        arr.(n) <- arr.(k);
        arr.(k) <- temp
      done
    in
    let array = Array.of_list lst in
    shuffle array;
    Array.to_list array

  let graph_testable = Alcotest.testable Rdf.Graph.pp Rdf.Graph.equal

  let property_based_equality_test =
    QCheck.Test.make ~name:"property based equality of graphs" Rdf_gen.triples
      (fun triples ->
        let a = Rdf.Graph.(empty |> add_seq (List.to_seq triples)) in
        let b = Rdf.Graph.(empty |> add_seq (List.to_seq @@ shuffle triples)) in
        Alcotest.check graph_testable
          "shuffling order of triples does not change graph" a b;
        true)

  let test_cases =
    [
      equal_unit_test; QCheck_alcotest.to_alcotest property_based_equality_test;
    ]
end

let () =
  Alcotest.run "rdf"
    [ ("Rdf.Term", TermTest.test_cases); ("Rdf.Graph", GraphTest.test_cases) ]
