(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let xml_to_string seq =
  let buffer = Buffer.create 60 in

  let output = Xmlm.make_output ~decl:false (`Buffer buffer) in

  (* Xmlm requirs a Dtd  *)
  Xmlm.output output (`Dtd None);

  seq |> Seq.iter (fun signal -> Xmlm.output output signal);

  String.of_seq @@ Buffer.to_seq buffer

let xml_of_string s =
  Xmlm.make_input (`String (0, s)) |> Rdf_xml.xmlm_input_to_seq

let encode_decode_graph =
  QCheck.Test.make ~count:50 ~name:"encode and decode a Graph" Rdf_gen.graph
    (fun graph ->
      let encoded = graph |> Rdf_xml.to_signals |> xml_to_string in

      Format.printf "%s\n" encoded;

      let decoded = encoded |> xml_of_string |> Rdf_xml.parse_to_graph in

      Alcotest.check Rdf_alcotest.graph
        "encoded and decoded graph is equal to original graph" graph decoded;
      true)

let () =
  let graph =
    Rdf.Graph.(
      empty
      |> add
           Rdf.Triple.(
             make
               (Subject.of_iri @@ Rdf.Iri.of_string "https://example.com")
               (Predicate.of_iri
               @@ Rdf.Iri.of_string "https://activitystream.com?hello")
               (Object.of_literal @@ Rdf.Literal.make_string ""))
      |> add
           Rdf.Triple.(
             make
               (Subject.of_blank_node @@ Rdf.Blank_node.of_string "a")
               (Predicate.of_iri
               @@ Rdf.Iri.of_string "https://activitystream.com?hello")
               (Object.of_blank_node @@ Rdf.Blank_node.of_string "b")))
  in

  let encoded = graph |> Rdf_xml.to_signals |> xml_to_string in

  Format.printf "%s\n" encoded;

  let decoded = encoded |> xml_of_string |> Rdf_xml.parse_to_graph in

  Format.printf "%a\n" Rdf.Graph.pp decoded;

  Alcotest.run "RDF/XML"
    [
      ( "Property-based encode and decode",
        List.map QCheck_alcotest.to_alcotest [ encode_decode_graph ] );
    ]

(* let () =
 *   let description =
 *     Rdf.Description.(
 *       empty (Rdf.Triple.Subject.of_iri @@ Rdf.Iri.of_string "urn:example")
 *       |> add
 *            (Rdf.Triple.Predicate.of_iri @@ Rdf.Namespace.rdf "type")
 *            (Rdf.Triple.Object.of_iri @@ Rdf.Iri.of_string "urn:example:2"))
 *   in
 * 
 *   Rdf_xml.signals_of_description description
 *   |> Seq.iter (Format.printf "%a\n" Xmlm.pp_signal) *)
